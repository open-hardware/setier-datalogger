****************************************************
DataLogger SETIER PHYSICO-CHIMIQUE V0.0 Hardware FR
****************************************************

     .. figure:: Image/Physico_Chemical/Hardware/Physico_final.JPG
	   :width: 887px
	   :align: center
	   :height: 754px
	   :alt: alternate text
	   :figclass: align-center

.. Attention::
	Nous vous invitons à vous referer à ce document pour assembler le datalogger-physico-chimique V1.02 associé au projet SETIER. Attention, lors de l'utilisation du datalogger, il est préconisé de faire en sorte que tous les capteurs physico-chimique (hors capteur de tempèrature) ne soit pas entiérement immergé dans l'eau.

	
.. attention::
	**SETIER est un projet participatif ouvert à tous, cependant l'assemblage des dataloggers associés requiere le respect des règles de sécurités associées à l'utilisation de cartes électroniques notamment. Les dataloggers SETIER doivent être assemblés dans un contexte professionel par des personnes ayant des connaissances en électronique. L'équipe SETIER ne peut en aucun cas être responsable de tout dommage matériels ou humain qui pourrait subvenir lors de l'assemblage ou de l'utilisation d'un datalogger SETIER. De plus, l'équipe SETIER ne pourra être portée responsable si le datalogger ne fonctionne pas à la fin de l'assemblage.You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.**
 
Données Techniques
********************
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|**Propriétés techniques des capteurs**	|**Specifications**  |Unités     |**Capteurs**       |Grandeur associée  |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Tension d'entrée (VCC)       	       	|3.3 à 5.5           |V          |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Intervalle de mesures          	|0 à 14              |unité pH   |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Température d'utilisation      	|0 à 60              |°C         |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Résolution                     	|0.1                 |unité pH   |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Temps de réponse               	|< 1                 |min        |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Durée de vie de la sonde       	|6                   |mois       |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Longueur de cable              	|5                   |m          |SEN0169-V2 Sensor  |**pH**             |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Tension d'entrée (VCC)         	|3 à 5               |V          |DFR0300 Sensor     |**Conductivité**   |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Interval de mesures            	|0 à 20              |mS/cm      |DFR0300 Sensor     |**Conductivité**   |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Température d'utilisation      	|0 à 40              |°C         |DFR0300 Sensor     |**Conductivité**   |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Durée de vie de la sonde       	|6                   |mois       |DFR0300 Sensor     |**Conductivité**   |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Longueur de cable              	|1                   |m          |DFR0300 Sensor     |**Conductivité**   |
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Tension d'entrée (VCC)         	|5                   |V          |SEN0464 Sensor     |**Potentiel Redox**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Interval de mesures            	|-2000 à 2000        |mV         |SEN0464 Sensor     |**Potentiel Redox**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Température d'utilisation      	|5 à 70              |°C         |SEN0464 Sensor     |**Potentiel Redox**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Précision (à 25°C)             	|10                  |mV         |SEN0464 Sensor     |**Potentiel Redox**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Tension d'entrée (VCC)         	|3.3 à 5             |V          |SEN0237 Sensor     |**Oxygène Dissous**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Intervalle de mesure           	|0 to 20             |mg/L       |SEN0237 Sensor     |**Oxygène Dissous**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Durée de vie de la sonde       	|12                  |mois       |SEN0237 Sensor     |**Oxygène Dissous**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Durée de vie de la membrane    	|1-2                 |mois       |SEN0237 Sensor     |**Oxygène Dissous**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Durée de vie de la soude       	|1                   |mois       |SEN0237 Sensor     |**Oxygène Dissous**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+
|Longueur de cable              	|2                   |m          |SEN0237 Sensor     |**Oxygène Dissous**|
+---------------------------------------+--------------------+-----------+-------------------+-------------------+


Assemblage du datalogger
**************************

Assemblage de la partie électronique 
======================================

La première partie consiste à assembler les différentes cartes électronique composant le datalogger.

Liste du matériel
------------------

+-------------------------------+--------------------+---------------------------------------------------+
|Nom                            |Nombre              |Image                                              |
+-------------------------------+--------------------+---------------------------------------------------+
|MKR MEM Shield                 |1                   |.. image:: Image/General/Electronic/MKR_SD.JPG     | 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR Connector Carrier          |1                   |.. image:: Image/General/Electronic/MKR_CARRIER.JPG| 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR WIFI 1010	                |1                   |.. image:: Image/General/Electronic/MKR_WIFI.JPG   | 
+-------------------------------+--------------------+---------------------------------------------------+
|Carte µSD                      |1                   |.. image:: Image/General/Electronic/SD.JPG         | 
+-------------------------------+--------------------+---------------------------------------------------+
|Hub I2C                        |1                   |.. image:: Image/General/Electronic/I2C_Hub.JPG    |
+-------------------------------+--------------------+---------------------------------------------------+
|Module RTC                     |1                   |.. image:: Image/General/Electronic/RTC.JPG        | 
+-------------------------------+--------------------+---------------------------------------------------+
|Diviseur de tension            |1                   |.. image:: Image/General/Electronic/VOLTAGE.JPG    | 
+-------------------------------+--------------------+---------------------------------------------------+
|Capteur de température         |1                   |.. image:: Image/General/Electronic/Temperature.JPG|
+-------------------------------+--------------------+---------------------------------------------------+
|Module Bornier à vis           |1                   |.. image:: Image/General/Electronic/Screw_Gr.JPG   |
+-------------------------------+--------------------+---------------------------------------------------+
|Resistance 4.7 kOhm            |1                   |.. image:: Image/General/Electronic/4.7kOhm.JPG    |
+-------------------------------+--------------------+---------------------------------------------------+
|Pile CR1220 lithium            |1                   |.. image:: Image/General/Electronic/Battery.JPG    |
+-------------------------------+--------------------+---------------------------------------------------+

.. sidebar:: Les cartes electroniques doivent être branchées dans le bon sens (Pin 5V sur Pin 5V par exemple).

             .. image:: Image/General/Electronic/WARNING_1.JPG

1.	Brancher la carte **MKR MEM Shield** sur la carte **MKR Connector Carrier**.

     .. figure:: Image/General/Electronic/MKR_MEM_On_CARRIER.JPG
	   :width: 1037px
	   :align: center
	   :height: 577px
	   :alt: alternate text
	   :figclass: align-center

2.	Connecter la carte **MKR WIFI 1010** sur la carte **MKR MEM Shield**. 

	 .. figure:: Image/General/Electronic/MKR_WIFI_On_CARRIER.JPG
	   :width: 996px
	   :align: center
	   :height: 502px
	   :alt: alternate text
	   :figclass: align-center

3.	Inserer la carte **µSD** dans la carte **MKR MEM SHIELD**.

	.. figure:: Image/General/Electronic/SD_On_CARRIER.JPG
	   :width: 925px
	   :align: center
	   :height: 461px
	   :alt: alternate text
	   :figclass: align-center
 
4.	Brancher sur le port "**Serial**" de la carte **MKR CARRIER** la carte **I2C Hub**.

	.. figure:: Image/Physico_Chemical/Hardware/4Divider_on_carrier.JPG
	   :width: 900px
	   :align: center
	   :height: 475px
	   :alt: alternate text
	   :figclass: align-center

5.	Prendre la carte **Grove RTC** et brancher son cable sur celle-ci.

	.. figure:: Image/Flowmeter/Hardware/RTC_Wire.JPG
	   :width: 500px
	   :align: center
	   :height: 343px
	   :alt: alternate text
	   :figclass: align-center
 
6.	Prendre la pile CR1220 et la brancher sur la carte RTC.

	.. figure:: Image/Physico_Chemical/Hardware/RTC_battery.JPG
	   :width: 618px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

7.	Brancher la carte **RTC** sur la carte **I2C Hub**.

	.. figure:: Image/Physico_Chemical/Hardware/wire_and_rtc_on_carrier.JPG
	   :width: 723px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

8.	Prendre la carte **diviseur de tension** et brancher son cable.

	.. figure:: Image/Flowmeter/Hardware/Voltage_Divider_Wire.JPG
	   :width: 900px
	   :align: center
	   :height: 492px
	   :alt: alternate text
	   :figclass: align-center

9.	Brancher l'autre extrémité du cable  **diviseur de tension** dans le port A5-A6 de la carte **MKR CONNECTOR CARRIER**.

	.. figure:: Image/Physico_Chemical/Hardware/voltage_divider_on_carrier.JPG
	   :width: 913px
	   :align: center
	   :height: 727px
	   :alt: alternate text
	   :figclass: align-center

10.	Prendre le capteur de temperature, le **module bornier à vis** et la **résistance 4.7 kOhm**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature.JPG
	   :width: 722px
	   :align: center
	   :height: 734px
	   :alt: alternate text
	   :figclass: align-center

11.	Brancher le cable noir du capteur de température sur le port **GND** de la carte **module bornier à vis**, le cable rouge sur le port **VCC**, et le cable blanc sur le port **D1**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_plug.JPG
	   :width: 744px
	   :align: center
	   :height: 737px
	   :alt: alternate text
	   :figclass: align-center

12.	Brancher une patte de la résistance avec le cable blanc sur le port **D1** et l'autre patte avec le cable rouge sur le port **VCC**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_resistor_plug.JPG
	   :width: 767px
	   :align: center
	   :height: 698px
	   :alt: alternate text
	   :figclass: align-center 

13.	Visser les borniers du **module bornier à vis** avec les différents cables branchés.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_resistor_plug_screw.JPG
	   :width: 862px
	   :align: center
	   :height: 744px
	   :alt: alternate text
	   :figclass: align-center 

14.	Brancher le cable au **module bornier à vis**, et l'autre coté sur le port **D0** de la carte **Arduino MKR CARRIER**.

	.. figure:: Image/Physico_Chemical/Hardware/temperature_carrier_plug.JPG
	   :width: 900px
	   :align: center
	   :height: 506px
	   :alt: alternate text
	   :figclass: align-center 

Assemblage mécanique
=====================

Liste du matériel
-------------------

+-----------------------------------------------+--------------------+------------------------------------------------------+
|Nom                                      	|Nombre              |Image                                                 |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Carte 4 Relais                           	|2                   |.. image:: Image/General/Electronic/relai.JPG         |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|SEN0169-V2(Capteur pH)                   	|1                   |.. image:: Image/General/Electronic/SEN0169.JPG       |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|DFR0300 (Capteur Conductivité)           	|1                   |.. image:: Image/General/Electronic/DFR0300.JPG       |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|SEN0464(Capteur ORP)                     	|1                   |.. image:: Image/General/Electronic/SEN0464.JPG       |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|SEN0237-A(Capteur O2)                    	|1                   |.. image:: Image/General/Electronic/SEN0237.JPG       |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Cables (Grove/Picot)                     	|4                   |.. image:: Image/General/Electronic/Grove_picot.JPG   |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Entretoise (M2 et M3) 20 mm de long      	|+/-10 de chaque     |.. image:: Image/General/Electronic/entretoise.JPG    |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Vis (M2 et M3)                           	|+/-10 de chaque     |.. image:: Image/General/Electronic/vis.JPG           |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Ecrou (M2 et M3)                         	|+/-10 de chaque     |.. image:: Image/General/Electronic/ecrou.JPG         |     
+-----------------------------------------------+--------------------+------------------------------------------------------+
|12 cm de cable rouge/noir diamètre 1,6 mm	|1 de chaque couleur |.. image:: Image/General/Electronic/cable-fin.JPG     |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|12 cm de cable rouge/noir diamètre 3 mm  	|1 de chaque couleur |.. image:: Image/General/Electronic/cable-épais.JPG   |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Cosse à sertir                           	|2                   |.. image:: Image/General/Electronic/Pod.JPG           |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Fiche banane rouge (diamètre extérieur 12 mm)  |1                   |.. image:: Image/General/Electronic/Red_Banana.JPG    |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Fiche banane noire (diamètre extérieur 12 mm)  |1                   |.. image:: Image/General/Electronic/Black_Banana.JPG  |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Presse-étoupe (diamètre extérieur 16 mm)       |2                   |.. image:: Image/General/Electronic/Cable_Gl.JPG      |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Boite 344 x 289 x 117mm                  	|1                   |.. image:: Image/General/Electronic/pc_Box.JPG        |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Support PVC                              	|1                   |.. image:: Image/General/Electronic/PVC_pc.JPG        |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Alimentation Voltcraft                   	|1                   |.. image:: Image/General/Electronic/Alimentation.JPG  |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Cable µ USB mâle/ USB-A mâle             	|1                   |.. image:: Image/General/Electronic/USB-Cable.JPG     |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Bouton poussoir (diamètre extérieur 14 mm)     |1                   |.. image:: Image/General/Electronic/Bouton.JPG        |
+-----------------------------------------------+--------------------+------------------------------------------------------+
|Cable Picot/Picot (rouge & noir)         	|10                  |.. image:: Image/General/Electronic/cable-picot.JPG   |
+-----------------------------------------------+--------------------+------------------------------------------------------+

**Pour cette partie, vous pouvez utiliser le fichier physico_chemical_datalogger_connexion.pdf afin de voir ou sont fixés et branchés les différentes cartes et capteurs.**

#.	Décoller le scotch marron sur chaque face des différentes parties de la boite de protection des **relais**.

        .. figure:: Image/General/Electronic/Relay_box_unstick.JPG
	   :width: 900px
	   :align: center
	   :height: 484px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la carte électronique **Carte 4 relais** et la fixer sur le support fourni à l'aide des vis (voir notice montage dans la boite du **carte 4 relais**).

        .. figure:: Image/General/Electronic/Relay_box_1.JPG
	   :width: 909px
	   :align: center
	   :height: 721px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre les 4 parties verticales de la boite de protection et les insérer dans le support. Ensuite, prendre la partie supérieure et la fixer à l'aide des entretoises et des vis fournies avec la carte électronique.

        .. figure:: Image/Physico_Chemical/Hardware/relay_box_closed.JPG
	   :width: 574px
	   :align: center
	   :height: 695px
	   :alt: alternate text
	   :figclass: align-center


#.	**Faire les étapes 1 à 4 deux fois afin d'avoir 2 carte 4 relais**.

#.	Prendre un morceau de bois, PVC, ou tout élément non conducteur, et le couper suivant le fichier "**Physico_Chemical_datalogger_hole.pdf**" afin de réaliser la plaque fixation des différents éléments du datalogger. Percer les trous afin de placer les différentes entretoises.

        .. figure:: Image/Physico_Chemical/Hardware/PVC_columns.JPG
	   :width: 900px
	   :align: center
	   :height: 595px
	   :alt: alternate text
	   :figclass: align-center

#.	Percer les trous, puis fixez les entretoises M2 et M3 avec des écrous pour les M3 et des vis pour les M2.

        .. figure:: Image/Physico_Chemical/Hardware/PVC-fixing.JPG
	   :width: 432px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la premiere carte **carte 4 relais** avec des entroises M3 sur la partie arrière de celle-ci ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/relay-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 580px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la seconde carte **carte 4 relais** sur les entroises M3 fixer à l'étape 7 ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). Pour la partie arrière, les fixer sur les entretoises déjà en place.

        .. figure:: Image/Physico_Chemical/Hardware/relay2-fixing.JPG
	   :width: 880px
	   :align: center
	   :height: 693px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **Arduino MKR CARRIER** sur les entretoises associées ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). 

        .. figure:: Image/Physico_Chemical/Hardware/arduino-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 650px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer le capteur de **température** sur les entretoises associées ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). 

        .. figure:: Image/Physico_Chemical/Hardware/temperature-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 662px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer le capteur de **Hub I2C**  sur les entretoises associées ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). 

        .. figure:: Image/Physico_Chemical/Hardware/4divider-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 637px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer le **module bornier à vis** sur les entretoises associées ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). 

        .. figure:: Image/Physico_Chemical/Hardware/push-button-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 749px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **RTC** sur les entretoises associées ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). 

        .. figure:: Image/Physico_Chemical/Hardware/RTC-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 750px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **diviseur de tension** sur les entretoises associées ("en suivant les indications du fichier physico_chemical_datalogger_hole.pdf"). 

        .. figure:: Image/Physico_Chemical/Hardware/voltage-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 719px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable de la carte **Hub I2C** aux 2 relais.

        .. figure:: Image/Physico_Chemical/Hardware/relay_connect.JPG
	   :width: 855px
	   :align: center
	   :height: 762px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le capteur de conductivité **DFR0300**. Brancher sur sa carte les deux cables.

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor.JPG
	   :width: 900px
	   :align: center
	   :height: 454px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le cable picot/Grove. Connecter le fil jaune sur le fil bleu du cable provenant de la carte du capteur **DFR0300**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre deux cables Picot/Picot (un noir et un rouge). Connecter le cable rouge sur le cable rouge provenant de la carte du capteur **DFR0300**, et connecter le cable noir sur le cable noir provenant de la carte du capteur **DFR0300**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotcher ces 3 cables ensembles.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable noir provenant du capteur sur l'entrée gauche de la première carte **relai**, premier relai. Ensuite, connecter le cable noir provenant du connecteur **MKR carrier** à l'entrée centrale de la première carte **relai**, premier relai.

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor_on_relay1.JPG
	   :width: 900px
	   :align: center
	   :height: 632px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable rouge provenant du capteur sur l'entrée gauche de la seconde carte **relai**, premier relai. Ensuite, connecter le cable rouge provenant du connecteur **MKR carrier** à l'entrée centrale de la seconde carte **relai**, premier relai.

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 616px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le connecteur **MKR Carrier** sur l'entrée **A3** de la carte **Arduino MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/conduc_sensor_on_carrier.JPG
	   :width: 900px
	   :align: center
	   :height: 664px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le **capteur d'oxygène**. Brancher sur sa carte les deux cables.

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor.JPG
	   :width: 900px
	   :align: center
	   :height: 438px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le cable picot/Grove. Connecter la partie jaune sur la partie bleu du cable provenant de la carte du capteur **O2**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre deux cables Picot/Picot (un noir et un rouge). Connecter le cable rouge sur le cable rouge provenant de la carte du capteur **O2**, et connecter le cable noir sur le cable noir provenant de la carte du capteur **O2**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotcher ces 3 cables ensembles.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable noir provenant du capteur sur l'entrée gauche de la première carte **relais**, second relais. Ensuite, connecter le cable noir provenant du connecteur **MKR carrier** à l'entrée centrale de la première carte **relais**, second relais.

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor_on_relay1.JPG
	   :width: 900px
	   :align: center
	   :height: 636px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable rouge provenant du capteur sur l'entrée gauche de la seconde carte **relais**, second relais. Ensuite, connecter le cable rouge provenant du connecteur **MKR carrier** à l'entrée centrale de la seconde carte **relais**, second relais.

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 695px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le connecteur **MKR Carrier** sur l'entrée **A2** de la carte **Arduino MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/O2_sensor_on_carrier.JPG
	   :width: 412px
	   :align: center
	   :height: 689px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le **capteur ORP**. Brancher sur sa carte le cable ci-contre. L'autre coté sera connecté plus tard.

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor.JPG
	   :width: 638px
	   :align: center
	   :height: 352px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le cable picot/grove. Connecter le cable jaune sur le cable bleu provenant de la carte du capteur **ORP**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre deux cables Picot/Picot (un noir et un rouge). Connecter le cable rouge sur le cable rouge provenant de la carte du capteur **ORP**, et connecter le cable noir sur le cable noir provenant de la carte du capteur **ORP**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotcher ces 3 cables ensembles.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable noir provenant du capteur sur l'entrée gauche de la première carte **relais**, troisième relais. Ensuite, connecter le cable noir provenant du connecteur **MKR carrier** à l'entrée centrale de la première carte **relais**, troisième relais.

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_on_relay1.JPG
	   :width: 900px
	   :align: center
	   :height: 660px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable rouge provenant du capteur sur l'entrée gauche de la seconde carte **relai**, troisième relai. Ensuite, connecter le cable rouge provenant du connecteur **MKR carrier** à l'entrée centrale de la seconde carte **relai**, troisième relai.

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 741px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le connecteur **MKR Carrier** sur l'entrée **A1** de la carte **Arduino MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_on_carrier.JPG
	   :width: 410px
	   :align: center
	   :height: 758px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le **capteur pH SEN0169-V2**. Brancher sur sa carte le cable ci-contre.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor.JPG
	   :width: 900px
	   :align: center
	   :height: 462px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre le cable picot/grove. Connecter le cable jaune sur le cable bleu provenant de la carte du capteur **pH**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_yellow_wire.JPG
	   :width: 722px
	   :align: center
	   :height: 748px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre deux cables Picot/Picot (un noir et un rouge). Connecter le cable rouge sur le cable rouge provenant de la carte du capteur **pH**, et connecter le cable noir sur le cable noir provenant de la carte du capteur **pH**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_wire.JPG
	   :width: 577px
	   :align: center
	   :height: 713px
	   :alt: alternate text
	   :figclass: align-center

#.	Scotcher ces 3 cables ensembles.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_scotch.JPG
	   :width: 900px
	   :align: center
	   :height: 717px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable noir provenant du capteur sur l'entrée gauche de la première carte **relais**, quatrième relais. Ensuite, connecter le cable noir provenant du connecteur **MKR carrier** à l'entrée centrale de la première carte **relais**, quatrième relais.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_on_relay1.JPG
	   :width: 885px
	   :align: center
	   :height: 759px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le cable rouge provenant du capteur sur l'entrée gauche de la seconde carte **relais**, quatrième relais. Ensuite, connecter le cable rouge provenant du connecteur **MKR carrier** à l'entrée centrale de la seconde carte **relais**, quatrième relais.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_on_relay2.JPG
	   :width: 900px
	   :align: center
	   :height: 757px
	   :alt: alternate text
	   :figclass: align-center

#.	Connecter le connecteur **MKR Carrier** sur l'entrée **A0** de la carte **Arduino MKR CARRIER**.

        .. figure:: Image/Physico_Chemical/Hardware/pH_sensor_on_carrier.JPG
	   :width: 556px
	   :align: center
	   :height: 735px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte du capteur **ORP** sur les entretoises prévues à cet effet ("physico_chemical_datalogger_hole.pdf").

        .. figure:: Image/Physico_Chemical/Hardware/ORP-fixing.JPG
	   :width: 900px
	   :align: center
	   :height: 750px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la boite et percer 5 trous : 3 pour les capteurs de conductivités-pH-O2 (trou de diamètre "**M12**"), et 2 pour le capteur de température et ORP (trou de diamètre "**M16**"). Attention, ces trous doivent être fait du coté où seront placés les relais dans la boîte. Utiliser une protection en bois pour ne pas abimer l'intérieur de la boite.

        .. figure:: Image/Physico_Chemical/Hardware/sensor_hole.JPG
	   :width: 900px
	   :align: center
	   :height: 524px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la boite et percer 2 trous de diamètre **M12**, de l'autre coté de la boite afin de placer les fiches bananes. Attention, ces trous doivent être fait du coté où sera placée la **carte Arduino** dans la boite.

        .. figure:: Image/Physico_Chemical/Hardware/banana_hole.JPG
	   :width: 900px
	   :align: center
	   :height: 575px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la boite et percer 1 trou de diamètre **M14**. Ce trou doit être placé du coté **voltage divider** de la boîte, afin d'y placer le **bouton poussoir**.

        .. figure:: Image/Physico_Chemical/Hardware/button_hole.JPG
	   :width: 900px
	   :align: center
	   :height: 384px
	   :alt: alternate text
	   :figclass: align-center

#.	Poser et fixer aux endroits prévus à cet effet la plaque de PVC dans la boite. 

        .. figure:: Image/Physico_Chemical/Hardware/PVC-inbox.JPG
	   :width: 900px
	   :align: center
	   :height: 551px
	   :alt: alternate text
	   :figclass: align-center

#.	Insérer les différents cables BNC des capteurs sur leurs cartes, en suivant cet ordre de haut en bas: pH,  O2, Conductivité (trou de diamètre 12 mm).

        .. figure:: Image/Physico_Chemical/Hardware/BNC_inbox.JPG
	   :width: 646px
	   :align: center
	   :height: 771px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer tous les cables BNC avec un joint et un écrou (fourni avec le capteur).

        .. figure:: Image/Physico_Chemical/Hardware/BNC_inbox_fixing.JPG
	   :width: 629px
	   :align: center
	   :height: 758px
	   :alt: alternate text
	   :figclass: align-center

#.	Couper 12 cm de **cable rouge de diamètre 3 mm**. Utiliser une **pince à dénuder** afin de dénuder 1 cm à chaque extrémité du cable.

        .. figure:: Image/Flowmeter/Hardware/red_wire_stripped.JPG
	   :width: 816px
	   :align: center
	   :height: 778px
	   :alt: alternate text
	   :figclass: align-center

#.	Couper 12 cm de **cable noir de diamètre 3 mm**. Utiliser une **pince à dénuder** afin de dénuder 1 cm à chaque extrémité du cable.

        .. figure:: Image/Flowmeter/Hardware/Black_wire_stripped.JPG
	   :width: 867px
	   :align: center
	   :height: 827px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre **1 cosse à sertir**. Utiliser une **pince à sertir** afin de le fixer sur une des extrémités du **cable rouge diamètre 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/red_wire_pod.JPG
	   :width: 792px
	   :align: center
	   :height: 682px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre **1 cosse à sertir**. Utiliser une **pince à sertir** afin de le fixer sur une des extrémités du **cable noir diamètre 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/black_wire_pod.JPG
	   :width: 740px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la fiche banane rouge dans son trou. Ensuite, connecter le cable rouge sur la fiche banane rouge, puis visser un écrou. Faire la même chose pour le cable noir et la fiche banane noire.

        .. figure:: Image/Physico_Chemical/Hardware/banana_plug-on-box.JPG
	   :width: 849px
	   :align: center
	   :height: 668px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher l'autre extrémité du cable rouge diamètre 3 mm avec l'autre extrémité du cable rouge venant du **diviseur de tension** sur le port VIN de la carte **MKR CARRIER**. Ensuite, brancher l'autre extrémité du cable noir diamètre 3 mm avec l'autre extrémité du cable noir venant du **diviseur de tension** sur le port GND de la carte **MKR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/input_Wire.JPG
	   :width: 824px
	   :align: center
	   :height: 674px
	   :alt: alternate text
	   :figclass: align-center

#.	Mettre en place le "**bouton poussoir**" dans son trou (trou de diamètre 12 mm), puis utiliser un écrou (fourni avec le bouton poussoir) pour le fixer.

        .. figure:: Image/Physico_Chemical/Hardware/push-button-inbox.JPG
	   :width: 884px
	   :align: center
	   :height: 701px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher les deux cables du **bouton poussoir** sur le **module bornier à vis** (Un des cables doit être branché sur le port VCC, l'autre sur le port D2), ensuite, vissez-le. Le branchement des deux cables est interchangeable.

        .. figure:: Image/Physico_Chemical/Hardware/push-button-connect.JPG
	   :width: 871px
	   :align: center
	   :height: 763px
	   :alt: alternate text
	   :figclass: align-center

#.	Mettre un presse-étoupe sur chaque trous restant (trou de diamètre 16 mm). Un est pour le cable du capteur **ORP**, l'autre pour le capteur de **température**.

        .. figure:: Image/Physico_Chemical/Hardware/stuffing-box.JPG
	   :width: 437px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Faire passer les cables du capteur "**ORP**" dans le presse-étoupe (de l'extérieur vers l'intérieur), puis visser ces cables sur le bornier à vis (voir image), puis connecter le bornier à vis sur la carte du capteur ORP. **Attention, les cables rouge et bleu ne doivent en aucun se toucher, sous risque de détériorer fortement le capteur.**

        .. figure:: Image/Physico_Chemical/Hardware/ORP_sensor_screw.JPG
	   :width: 900px
	   :align: center
	   :height: 648px
	   :alt: alternate text
	   :figclass: align-center

#.	Faire passer le capteur "**température**" dans le presse-étoupe.

        .. figure:: Image/Physico_Chemical/Hardware/sensor_plug.JPG
	   :width: 900px
	   :align: center
	   :height: 758px
	   :alt: alternate text
	   :figclass: align-center

#.	Fermer la boite. Le datalogger Physico-chimique est prêt à être utilisé. **Soyez attentif, le datalogger nécessite une alimentation en 12V continu, utilisez donc une source de courant adaptée**.

        .. figure:: Image/Physico_Chemical/Hardware/box_close.JPG
	   :width: 900px
	   :align: center
	   :height: 520px
	   :alt: alternate text
	   :figclass: align-center


