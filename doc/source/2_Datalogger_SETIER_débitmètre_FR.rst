**********************************************
DataLogger SETIER DEBITMETRE V0.0 Hardware FR
**********************************************

     .. figure:: Image/Flowmeter/Hardware/Flowmeter_final.JPG
	   :width: 900px
	   :align: center
	   :height: 676px
	   :alt: alternate text
	   :figclass: align-center

.. attention::
	Nous vous invitons à vous referer à ce document pour assembler le datalogger-débimètre V0.00 associé au projet SETIER.
	
.. attention::
	**SETIER est un projet participatif ouvert à tous, cependant l'assemblage des dataloggers associés requiere le respect des règles de sécurités associées à l'utilisation de cartes électroniques notamment. Les dataloggers SETIER doivent être assemblés dans un contexte professionel par des personnes ayant des connaissances en électronique. L'équipe SETIER ne peut en aucun cas être responsable de tout dommage matériels ou humain qui pourrait subvenir lors de l'assemblage ou de l'utilisation d'un datalogger SETIER. De plus, l'équipe SETIER ne pourra être portée responsable si le datalogger ne fonctionne pas à la fin de l'assemblage.You may redistribute and modify this documentation and make products using it under the terms of the CERN-OHL-P v2 (https:/cern.ch/cern-ohl). This documentation is distributed WITHOUT ANY EXPRESS OR IMPLIED WARRANTY, INCLUDING OF MERCHANTABILITY, SATISFACTORY QUALITY AND FITNESS FOR A PARTICULAR PURPOSE. Please see the CERN-OHL-P v2 for applicable conditions.**

	
Données Techniques
*******************

Données représentant les différents paramètres associées à l'utilisation de la centrale débitmètrique.

+-------------------------------+--------------------+-----------+
|**Paramètres**                 |**Specifications**  |Unités     |
+-------------------------------+--------------------+-----------+
|Tension d'entrée (VCC)         |7 - 15              |V          |
+-------------------------------+--------------------+-----------+
|Intervalle de mesures          |100 - 1500          |mm         |
+-------------------------------+--------------------+-----------+
|Précision de mesures           |1                   |mm         |             
+-------------------------------+--------------------+-----------+
|Résolution                     |0.1                 |mm         |
+-------------------------------+--------------------+-----------+
|Pourcentage d'erreur           |+/- 0.1             |%          |
+-------------------------------+--------------------+-----------+
|Résolution (T°C)               |0.1                 |°C         |
+-------------------------------+--------------------+-----------+
|Erreur de température          |+/-1                |°C         |
+-------------------------------+--------------------+-----------+
|Température d'utilisation      |-20 - 80            |°C         |
+-------------------------------+--------------------+-----------+
|Fréquence de mesure            |30                  |Hz         |
+-------------------------------+--------------------+-----------+
|Cône d'émission ultrason       |12 +/-2             |°          |
+-------------------------------+--------------------+-----------+
|Etanchéité du capteur          |IP65                | -         |
+-------------------------------+--------------------+-----------+
|Interface de communication     |RS485               | -         |
+-------------------------------+--------------------+-----------+
|Stockage des données           |micro SD card       | -         |
+-------------------------------+--------------------+-----------+

Assemblage du datalogger
**************************

Etape 1 : Assemblage de la partie électronique
===============================================

La première partie consiste à assembler les différentes cartes électroniques composant le datalogger.

Liste du matériel
-------------------

+-------------------------------+--------------------+---------------------------------------------------+
|Nom                            |Nombre              |Image                                              | 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR MEM Shield                 |1                   |.. image:: Image/General/Electronic/MKR_SD.JPG     | 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR Connector Carrier          |1                   |.. image:: Image/General/Electronic/MKR_CARRIER.JPG| 
+-------------------------------+--------------------+---------------------------------------------------+
|MKR WIFI 1010	                |1                   |.. image:: Image/General/Electronic/MKR_WIFI.JPG   | 
+-------------------------------+--------------------+---------------------------------------------------+
|Carte µSD                      |1                   |.. image:: Image/General/Electronic/SD.JPG         | 
+-------------------------------+--------------------+---------------------------------------------------+
|Module de communication RS485  |1                   |.. image:: Image/General/Electronic/RS485.JPG      | 
+-------------------------------+--------------------+---------------------------------------------------+
|Module RTC                     |1                   |.. image:: Image/General/Electronic/RTC.JPG        | 
+-------------------------------+--------------------+---------------------------------------------------+
|Diviseur de tension            |1                   |.. image:: Image/General/Electronic/VOLTAGE.JPG    | 
+-------------------------------+--------------------+---------------------------------------------------+
|Pile CR1220 lithium            |1                   |.. image:: Image/General/Electronic/Battery.JPG    | 
+-------------------------------+--------------------+---------------------------------------------------+

.. sidebar:: Les cartes electroniques doivent être branchées dans le bon sens (Pin 5V sur Pin 5V par exemple).

             .. image:: Image/General/Electronic/WARNING_1.JPG
	      
1.	Brancher la carte **MKR MEM Shield** sur la carte **MKR Connector Carrier**.

        .. figure:: Image/General/Electronic/MKR_MEM_On_CARRIER.JPG
	  :width: 1037px
	  :align: center
	  :height: 577px
          :alt: alternate text
          :figclass: align-center

2.	Connecter la carte **MKR WIFI 1010** sur la carte **MKR MEM Shield**.  

	 .. figure:: Image/General/Electronic/MKR_WIFI_On_CARRIER.JPG
	   :width: 996px
	   :align: center
	   :height: 502px
	   :alt: alternate text
	   :figclass: align-center

3.	Inserer la carte **µSD** dans la carte **MKR MEM SHIELD**.

	.. figure:: Image/General/Electronic/SD_On_CARRIER.JPG
	   :width: 925px
	   :align: center
	   :height: 461px
	   :alt: alternate text
	   :figclass: align-center

.. sidebar::  Le câble doit être branché dans le bon sens.

             .. image:: Image/General/Electronic/WARNING_2.JPG

4.	Prendre la carte *module de communication RS485** et brancher son cable dessus.

        .. figure:: Image/Flowmeter/Hardware/RS485_Wire.JPG
	   :width: 780px
	   :align: center
	   :height: 560px
	   :alt: alternate text
	   :figclass: align-center	

5.	Brancher l'autre côté du câble de la carte **module de communication RS485** vers le port **SERIAL** de la carte **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/RS485_On_CARRIER.JPG
	   :width: 704px
	   :align: center
	   :height: 385px
	   :alt: alternate text
	   :figclass: align-center

6.	Prendre la carte **Grove RTC** et brancher son câble dessus.

        .. figure:: Image/Flowmeter/Hardware/RTC_Wire.JPG
	   :width: 500px
	   :align: center
	   :height: 343px
	   :alt: alternate text
	   :figclass: align-center

7.	Prendre la pile CR1220 et la brancher sur la carte RTC.

	.. figure:: Image/Physico_Chemical/Hardware/RTC_battery.JPG
	   :width: 15cm
	   :align: center
	   :alt: RTC_battery
	   :figclass: align-center


8.	Brancher l'autre coté du câble de la carte **GROVE RTC** vers le port **TWI** de la carte **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/RTC_On_CARRIER.JPG
	   :width: 693px
	   :align: center
	   :height: 416px
	   :alt: alternate text
	   :figclass: align-center

9.	Prendre la carte **diviseur de tension** et brancher son câble.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_Wire.JPG
	   :width: 1023px
	   :align: center
	   :height: 561px
	   :alt: alternate text
	   :figclass: align-center

10.	Brancher l'autre extrémité du câble  **diviseur de tension** dans le port A5-A6 de la carte **MKR CONNECTOR CARRIER**.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_On_CARRIER.JPG
	   :width: 940px
	   :align: center
	   :height: 371px
	   :alt: alternate text
	   :figclass: align-center

Etape 2 : Assemblage mécanique
===============================

Liste du Matériel
-------------------

+----------------------------------------------------+--------------------+----------------------------------------------------+
|Nom                                                 |Nombre              |Image                                               |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Support PVC                                         |1                   |.. image:: Image/General/Electronic/PVC_Flow.JPG    |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Boite 201x163x98mm                                  |1                   |.. image:: Image/General/Electronic/flow_Box.JPG    |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|SEN0358 capteur (distance)                          |1                   |.. image:: Image/General/Electronic/SEN0358.JPG     |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Entretoise (M2 et M3) 20 mm de long                 |+/-10 de chaque     |.. image:: Image/General/Electronic/entretoise.JPG  |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Vis (M2 et M3)                                      |+/-10 de chaque     |.. image:: Image/General/Electronic/vis.JPG         |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Ecrou (M2 et M3)                                    |+/-10 de chaque     |.. image:: Image/General/Electronic/ecrou.JPG       |   
+----------------------------------------------------+--------------------+----------------------------------------------------+
|12 cm cable multibrin rouge/noir diamètre 1,6 mm    |1 de chaque couleur |.. image:: Image/General/Electronic/cable-fin.JPG   |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|12 cm cable multibrin rouge/noir  diamètre 3 mm     |1 de chaque couleur |.. image:: Image/General/Electronic/cable-épais.JPG |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Cosse à sertir                                      |2                   |.. image:: Image/General/Electronic/Pod.JPG         |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Fiche banane rouge (diamètre extérieur 12 mm)       |1                   |.. image:: Image/General/Electronic/Red_Banana.JPG  |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Fiche banane noir (diamètre extérieur 12 mm)        |1                   |.. image:: Image/General/Electronic/Black_Banana.JPG|
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Alimentation voltcraft                              |1                   |.. image:: Image/General/Electronic/Alimentation.JPG|
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Cable µ USB mâle/ USB-A mâle                        |1                   |.. image:: Image/General/Electronic/USB-Cable.JPG   |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Bouton poussoir (diamètre extérieur 14 mm)          |1                   |.. image:: Image/General/Electronic/Bouton.JPG      |
+----------------------------------------------------+--------------------+----------------------------------------------------+
|Module bornier à vis                                |1                   |.. image:: Image/General/Electronic/Screw_Gr.JPG    |
+----------------------------------------------------+--------------------+----------------------------------------------------+

#.	Prendre un morceau de bois, PVC, ou tout élément non conducteur, et le couper suivant le fichier "**Flowmeter_datalogger_hole.pdf**" afin de réaliser la plaque de fixation des différents éléments du datalogger. Percer les trous afin de placer les différentes entretoises, que vous fixerez.

        .. figure:: Image/Flowmeter/Hardware/PVC_Hole.JPG
	   :width: 633px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center


#.	Percer un trou en suivant le modèle "**Flowmeter_datalogger_hole.pdf**" dans la boîte **201x163x98mm**.

        .. figure:: Image/Flowmeter/Hardware/Box_Hole.JPG
	   :width: 663px
	   :align: center
	   :height: 278px
	   :alt: alternate text
	   :figclass: align-center


#.	Insérer le capteur **SEN0358** dans le trou percé précédemment, et le fixer à l'aide d'écrous, de chaque coté de la boite.

        .. figure:: Image/Flowmeter/Hardware/SEN_0358_FIXED.JPG
	   :width: 800px
	   :align: center
	   :height: 400px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la carte **Arduino MKR Carrier** sur les entretoise prévues à cet effet, à l'aide de vis.

        .. figure:: Image/Flowmeter/Hardware/arduino_fix.JPG
	   :width: 590px
	   :align: center
	   :height: 675px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer à l'aide de vis la carte **RTC** sur les entretoise prévues à cet effet.

        .. figure:: Image/Flowmeter/Hardware/rtc_fix.JPG
	   :width: 650px
	   :align: center
	   :height: 706px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer à l'aide de vis le **module bornier à vis** sur les entretoise prévues à cet effet (au dessus de la carte RTC).

        .. figure:: Image/Flowmeter/Hardware/push_fix.JPG
	   :width: 626px
	   :align: center
	   :height: 712px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer la **plaque de PVC** sur le fond de la boîte, avec des vis, en faisant passer le câble du capteur par le trou.

        .. figure:: Image/Flowmeter/Hardware/SEN_0358_WIRE.JPG
	   :width: 605px
	   :align: center
	   :height: 724px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre un câble rouge indépendant, ainsi que le câble marron du capteur **SEN0358**, et les brancher tous deux sur le port **VOL** de la carte **diviseur de tension**. Les visser.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_connexion_1.JPG
	   :width: 867px
	   :align: center
	   :height: 681px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre un câble noir indépendant, ainsi que le câble noir du capteur **SEN0358**, et les brancher tous deux sur le port **GND** de la carte **diviseur de tension**. Les visser.

        .. figure:: Image/Flowmeter/Hardware/Voltage_Divider_connexion_2.JPG
	   :width: 800px
	   :align: center
	   :height: 600px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre la carte **module de communication RS485** et brancher les fils du capteur **SEN0358**  à l'intérieur en utlisant un tournevis plat : Le fil bleu doit être branché sur la borne B et le blanc doit être branché sur la borne A.

        .. figure:: Image/Flowmeter/Hardware/RS485_connexion.JPG
	   :width: 776px
	   :align: center
	   :height: 710px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer à l'aide de vis la carte ** module de communication RS485** sur les entretoises prévues à cet effet.

        .. figure:: Image/Flowmeter/Hardware/RS485_fix.JPG
	   :width: 900px
	   :align: center
	   :height: 706px
	   :alt: alternate text
	   :figclass: align-center

#.	Fixer à l'aide de vis la carte **diviseur de tension** sur les entretoises prévues à cet effet (au dessus de la carte **module de communication RS485**).

        .. figure:: Image/Flowmeter/Hardware/divider_fix.JPG
	   :width: 900px
	   :align: center
	   :height: 700px
	   :alt: alternate text
	   :figclass: align-center

#.	Perçer deux trous au diamètre 12 mm. Prendre une **fiche banane rouge** et une **fiche banane noire**. Les insérer puis les fixer dans les 2 trous présents sur la boite.

        .. figure:: Image/Flowmeter/Hardware/banana_plug.JPG
	   :width: 599px
	   :align: center
	   :height: 743px
	   :alt: alternate text
	   :figclass: align-center

#.	Couper 12cm de **câble rouge diamètre 3 mm**. Utiliser une **pince à dénuder** afin de dénuder 1 cm à chaque extrémité du câble.

        .. figure:: Image/Flowmeter/Hardware/red_wire_stripped.JPG
	   :width: 816px
	   :align: center
	   :height: 778px
	   :alt: alternate text
	   :figclass: align-center

#.	Couper 12cm de **câble noir diamètre 3 mm**. Utiliser une **pince à dénuder** afin de dénuder 1 cm à chaque extrémité du câble.

        .. figure:: Image/Flowmeter/Hardware/Black_wire_stripped.JPG
	   :width: 867px
	   :align: center
	   :height: 827px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre une **cosse à sertir**. Utiliser une pince à sertir afin de la fixer sur une des extrémités du **câble rouge diamètre 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/red_wire_pod.JPG
	   :width: 792px
	   :align: center
	   :height: 682px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre une **cosse à sertir**. Utiliser une pince à sertir afin de la fixer sur une des extrémités du **câble Noir diamètre 3 mm**.

        .. figure:: Image/Flowmeter/Hardware/black_wire_pod.JPG
	   :width: 740px
	   :align: center
	   :height: 729px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher la **cosse** sertie du **câble rouge diamètre 3 mm** sur la **fiche banane rouge** à l'aide d'un écrou. 

        .. figure:: Image/Flowmeter/Hardware/red_pod_on_banana_plug.JPG
	   :width: 705px
	   :align: center
	   :height: 738px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher la **cosse** sertie du **câble noir diamètre 3 mm** sur la **fiche banane noire** à l'aide d'un écrou. 

        .. figure:: Image/Flowmeter/Hardware/black_pod_on_banana_plug.JPG
	   :width: 687px
	   :align: center
	   :height: 733px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre l'autre extrémité du **câble rouge diamètre 3 mm**, et le brancher sur le port Vin de la carte **MKR CONNECTOR CARRIER** avec l'autre extrémité du câble rouge épais raccordé à la fiche banane rouge.

        .. figure:: Image/Flowmeter/Hardware/Arduino_CARRIER_connexion.JPG
	   :width: 660px
	   :align: center
	   :height: 730px
	   :alt: alternate text
	   :figclass: align-center

#.	Prendre l'autre extrémité du **câble noir diamètre 3 mm**, et le brancher sur le port GND de la carte **MKR CONNECTOR CARRIER** avec l'autre extrémité du câble noir épais raccordé à la fiche banane noire.

        .. figure:: Image/Flowmeter/Hardware/Arduino_CARRIER_connexion_2.JPG
	   :width: 574px
	   :align: center
	   :height: 723px
	   :alt: alternate text
	   :figclass: align-center

#.	Percer un trou de 14 mm de diamètre sur le coté de la boîte, et y insérer le **bouton poussoir**. Utiliser un écrou afin de le fixer.

        .. figure:: Image/Flowmeter/Hardware/push_button_fix.JPG
	   :width: 599px
	   :align: center
	   :height: 737px
	   :alt: alternate text
	   :figclass: align-center

#.	Brancher les deux câbles du **bouton poussoir** sur le **module bornier à vis** (Un des câbles doit être branché sur le port VCC, l'autre sur le port D2), ensuite, vissez le.

        .. figure:: Image/Flowmeter/Hardware/push_button_connect.JPG
	   :width: 865px
	   :align: center
	   :height: 712px
	   :alt: alternate text
	   :figclass: align-center

#.	Fermer la boite. Le Datalogger Débitmètre est prêt à être utilisé. **Soyez attentif, le datalogger nécessite une alimentation en 12V continu, utilisez donc une source de courant adaptée**.

     .. figure:: Image/Flowmeter/Hardware/Flowmeter_final2.JPG
	   :width: 583px
	   :align: center
	   :height: 733px
	   :alt: alternate text
	   :figclass: align-center

