/**************************************************************************************************************/
/*************** DATALOGGER CONFIGURATION FILE ****************************************************************/
/**************************************************************************************************************/

/************* CHOOSE THE DATA LOGGER NAME   ******************************************************************/
/********* remplacer le nom entre guillemets par le votre attention pas plus de 8 caracteres ******************/
/********* keep the inverted commas ***************************************************************************/
/********* The wifi network created will bear the name of the datalogger **************************************/

#define STATION_NAME  "Water_l"  // For example, the name of my station is Water_l
/************* CHOOSE THE MEASUREMENT FREQUENCY ***************************************************************/
/******** DELETE THE TWO SLASHES AT THE BEGINNING OF THE LINE FOR THE CHOSEN FREQUENCY (//) *******************/
/******** Single choice ***************************************************************************************/

#define Ma_Cadence   1 // 1 measurement every minute (not suitable for chemical-physical data loggers)
//#define Ma_Cadence   5 // 1 measurement every 5 minutes
//#define Ma_Cadence  15 // 1 measurement every 15 minutes
// #define Ma_Cadence  30 // 1 measurement every half hour 
//#define Ma_Cadence  60 // 1 measurement per hour
// #define Ma_Cadence  24 // 1 measurement per day at midday

/************* CHOICE 1 WATER HEIGHT DATALOGGER ***************************************************************/
/********** DELETE THE TWO LEADING SLASHES FROM THE FOLLOWING LINES (//)***************************************/
/******* Enter the distance from the end of the sensor to the bottom of the channel (value at zero flow) in mm****/
/******* Please note that the minimum distance that can be measured by the sensor is 100 mm. ******************/
/******* Enter the channel depth in mm*************************************************************************/

#define TYPE_DATALOGGER_HAUTEUR_EAU 1
#define Ht_Debit_NUL 700 // ex my distance is 300 mm the line is written #define Ht_Debit_NUL 300
#define Profondeur_Canal 300 //e.g. my venturi channel depth is 300 mm 
 

/************* CHOICE 2 PHYSICO CHEMICAL DATALOGGER ***********************************************************/
/********** DELETE THE TWO LEADING SLASHES FROM THE FOLLOWING LINES (//)***************************************/

//#define TYPE_DATALOGGER_PHYSICO_CHIMIQUE 2
//#define Capteur_PH_A0 1  //replace 0 (no sensor) by 1 if PH sensor present
//#define Capteur_Redox_A1 1  //replace 0 (no sensor) by 1 if ORP sensor present
//#define Capteur_O2_A2 1  //replace 0 (no sensor) by 1 if O2 sensor present
//#define Capteur_Conducti_A3 1  //replace 0 (no sensor) by 1 if Conductivity sensor present


/************* CHOICE 3 ENERGY DATA LOGGER ********************************************************************/
/********** SUPPRIMER LES DEUX SLASH DE DEBUT DES 5 LIGNES SUIVANTES (//)**************************************/

//#define TYPE_DATALOGGER_ENERGIE 3
//#define Capteur_Energie_A0 20  //replace the 0 (no sensor) by 5 if it is a 5A sensor / 10 for 10A / 20 for 20A
//#define Capteur_Energie_A1 0  //replace the 0 by 5 if it's a 5A sensor / 10 for 10A / 20 for 20A
//#define Capteur_Energie_A2 0  //replace the 0 by 5 if it's a 5A sensor / 10 for 10A / 20 for 20A
//#define Capteur_Energie_A3 0  //replace the 0 by 5 if it's a 5A sensor / 10 for 10A / 20 for 20A





/************* CHOICE 4 SLUDGE HEIGHT DATA LOGGER **************************************************************/
/********** DELETE THE TWO LEADING SLASHES FROM THE FOLLOWING LINES (//)****************************************/
/******* Enter the distance between the end of the sensor and the bottom of the tank (value when the tank is empty) in cm.****/
/******* Please note that the minimum distance that can be measured by the sensor is 35 cm. ********************/
/******* Please note that the maximum distance that can be measured by the sensor is 550 cm. *******************/
/***************************************************************************************************************/

//#define TYPE_DATALOGGER_HAUTEUR_BOUE 4
//#define Ht_a_Vide 700 
 
