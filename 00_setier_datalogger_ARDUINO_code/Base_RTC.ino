// RTC Card: GROVE RTC DS1307 Module ref 101020013 (TWI connector)
// internal RTC of the SAMD21 microcontroller (Zero_RTC)

void Alarm_Zero_RTC(void) // Do not remove, it is useful
{  
}

void Init_DS1307_RTC (void) 
{  
  if (! DS1307_RTC.begin()) 
  {
    Serial.println("No DS1307 found");
    return;
  }
  // Following line sets the RTC to the date & time this sketch was compiled
  // DS1307_RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
  // This line sets the RTC with an explicit date & time, for example to set
  // January 21, 2014 at 3am you would call:
  //DS1307_RTC.adjust(DateTime(2022, 2, 23, 17, 22, 0));
  if (!New_Compil.read()) // If there was a compilation, the DS1307 RTC is automatically set to the PC time
  {
    DS1307_RTC.adjust(DateTime(F(__DATE__), F(__TIME__)));
    DS1307_Time = DS1307_RTC.now(); // Setting the time on the DS1307 RTC  
    Serial.print("The DS1307 RTC has been set to the computer's time: ");
    Serial.print(DS1307_Time.year(), DEC);
    Serial.print('/');
    Serial.print(DS1307_Time.month(), DEC);
    Serial.print('/');
    Serial.print(DS1307_Time.day(), DEC);
    Serial.print(" ");
    Serial.print(DS1307_Time.hour(), DEC);
    Serial.print(':');
    Serial.print(DS1307_Time.minute(), DEC);
    Serial.print(':');
    Serial.println(DS1307_Time.second(), DEC);   
  }
  if (! DS1307_RTC.isrunning()) 
  {
    Serial.println("RTC is NOT running!");    
  }  
}

void Init_Zero_RTC (void)
{
  Zero_RTC.begin();
  Alarm_Zero_RTC_Config;
}

void Alarm_Zero_RTC_Config (void) // For reference, the RTC interrupt priority is set to 0x00
{
  // Setting the time on the zero RTC
  DS1307_Time = DS1307_RTC.now();  
  Zero_RTC.setTime(DS1307_Time.hour(), DS1307_Time.minute(), DS1307_Time.second());
  Zero_RTC.setDate(DS1307_Time.day(), DS1307_Time.month(), DS1307_Time.year());

  // Alarm settings
  switch (Ma_Cadence)
  {
    case 1: // 1 measurement per minute
      Zero_RTC.setAlarmSeconds(30);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_SS);
      break;
    case 5: // 1 measurement every 5 minutes
      int AMin;
      int Res;
      Zero_RTC.setAlarmSeconds(0);
      AMin = Zero_RTC.getMinutes();
      do
      {
        AMin++;
        Res = AMin % 5;
      } while (Res != 0);
      if (AMin == 60)      
        Zero_RTC.setAlarmMinutes(0);
      else 
        Zero_RTC.setAlarmMinutes(AMin);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 15: // 1 measurement every 15 minutes
      Zero_RTC.setAlarmSeconds(0);
      if (Zero_RTC.getMinutes() >= 0 && Zero_RTC.getMinutes() < 15)
          Zero_RTC.setAlarmMinutes(15);
      else
      {
        if (Zero_RTC.getMinutes() >= 15 && Zero_RTC.getMinutes() < 30)
          Zero_RTC.setAlarmMinutes(30);
        else
        {
          if (Zero_RTC.getMinutes() >= 30 && Zero_RTC.getMinutes() < 45)
            Zero_RTC.setAlarmMinutes(45);
          else
            Zero_RTC.setAlarmMinutes(0);
        }
      }
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 30: // 1 measurement every 30 minutes at 00 and 30
      Zero_RTC.setAlarmSeconds(0);
      if (Zero_RTC.getMinutes() >= 0 && Zero_RTC.getMinutes() < 30)
        Zero_RTC.setAlarmMinutes(30);
      else
        Zero_RTC.setAlarmMinutes(0);  
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 60: // 1 measurement every hour on the hour
      Zero_RTC.setAlarmSeconds(0);
      Zero_RTC.setAlarmMinutes(0);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
      break;
    case 24: // 1 measurement per day at noon
      Zero_RTC.setAlarmSeconds(0);
      Zero_RTC.setAlarmMinutes(0);
      Zero_RTC.setAlarmHours(12);
      Zero_RTC.enableAlarm(Zero_RTC.MATCH_HHMMSS);
      break;
  }    
    Zero_RTC.attachInterrupt(Alarm_Zero_RTC);  
}

void Alarm_Zero_RTC_Wifi (void) // The timer is set to Wifi_length in minutes
{     // For reference, the RTC interrupt priority is set to 0x00
  int WifiEnd;
  // Setting the time on the zero RTC
  DS1307_Time = DS1307_RTC.now();  
  Zero_RTC.setTime(DS1307_Time.hour(), DS1307_Time.minute(), DS1307_Time.second());
  Zero_RTC.setDate(DS1307_Time.day(), DS1307_Time.month(), DS1307_Time.year());

  // Alarm settings
  Zero_RTC.setAlarmSeconds(0);
  WifiEnd = Zero_RTC.getMinutes() + Wifi_length;
      
  if (WifiEnd < 60)
      Zero_RTC.setAlarmMinutes(WifiEnd);
  else
  {      
      WifiEnd = WifiEnd - 60;
      Zero_RTC.setAlarmMinutes(WifiEnd);
  } 
  Zero_RTC.enableAlarm(Zero_RTC.MATCH_MMSS);
     
  Zero_RTC.attachInterrupt(Zero_RTC_Interrupt_Pg);  
}
