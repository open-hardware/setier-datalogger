/* File containing functions for the SD card: MKR MEM Shield ref: ASX00008 */

void Init_SD_Card()
{
  if (!SD.begin(SD_Pin)) // If no SD card is detected
  {
    Serial.println(F("SD card not detected"));
    SD_OK = false;
  }
  else
  {  
    Serial.println(F("SD card found and initialized")); 
    SD_OK = true;
  } 
}

void Write_WIFI_SD() // Writes WiFi connection logs to track maintenance schedule dates
{
    DS1307_Time = DS1307_RTC.now(); // Setting the time on the DS1307 RTC     
    SD_File = SD.open("LogWifi.txt", FILE_READ);
    if (!SD_File) // Opening the file
    {
       SD_File = SD.open("LogWifi.txt", FILE_WRITE);                
       SD_File.println(F("WiFi connection log file"));
       SD_File.println(F("START_DATA"));           
    } 
    SD_File.close();
    SD_File = SD.open("LogWifi.txt", FILE_WRITE);   
    SD_File.print(DS1307_Time.year(), DEC);
    SD_File.print('/');
    if (DS1307_Time.month() < 10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.month(), DEC);
    SD_File.print('/');
    if (DS1307_Time.day() < 10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.day(), DEC);
    SD_File.print(" ");
    if (DS1307_Time.hour() < 10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.hour(), DEC);
    SD_File.print(':');
    if (DS1307_Time.minute() < 10)
      SD_File.print('0');
    SD_File.print(DS1307_Time.minute(), DEC);
    SD_File.println();
    SD_File.close();
}
