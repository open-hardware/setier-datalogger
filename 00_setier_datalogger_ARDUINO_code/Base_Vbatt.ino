// Battery voltage reading board: GROVE voltage divider module ref 104020000 (connector A5/A6)


void Get_Vbatt(float *Vofbatt) //lecture tension battery
{
    long Vbatt_Value=0;
    long  sum=0;
    analogReadResolution(12);
    for(int i=0;i<10;i++)
    {
        Vbatt_Value=analogRead(Vbatt_Pin);
        sum=Vbatt_Value+sum;
        delay(2);
    }
    sum=sum/10;
    *Vofbatt= sum*33;
    *Vofbatt = *Vofbatt/4095; // resolution 12 bits (4095 = 2power12 - 1)
    *Vofbatt = *Vofbatt * 1.52;// consideration of the analog input divider bridge on the connector board 
    return;
  
}
