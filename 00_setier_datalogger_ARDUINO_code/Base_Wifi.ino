void Awake_Wifi()
{
  int NbrFic;                         // Number of files on SD card (for Wifi)
  float EspTotal;                     // SD card capacity (for Wifi)
  float EspLibre;                     // Free space on SD card (for Wifi)
  float EspOcc;                       // Occupied space on SD card (for Wifi)
  String Cwifi;                       // Date and time of last wifi connection
  digitalWrite(STATE_LED_Pin, HIGH);
  Zero_RTC.detachInterrupt(); 
  WIFI_RTC_STATE = true ; 
  Cwifi = Get_Cwifi();  // Retrieve the date of the last wifi connection
   // Update SD card data
  EspTotal = etatSD();   // SD card capacity in GB
  EspOcc = espOccSD();   // Occupied space on SD card in GB
  NbrFic = inspectSD();  // Number of files on SD card
  String NomFicSD[NbrFic]; // NbrFic: the number of files on the SD card is obtained during wifi setup
  float SizeFicSD[NbrFic];
  File entry;         
  SD_File=SD.open("/");
  byte i =0;
  // Variables for physico-chemical datalogger
  #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE)
    boolean PH_Aff = false;
    boolean PH_Mes = false;
    boolean Redox_Aff = false;
    boolean Redox_Mes = false;
    boolean O2_Aff = false;
    boolean O2_Mes = false;
    boolean EC_Aff = false;
    boolean EC_Mes = false;
    boolean Modif_Coeff = false;
    String Coeff;
  #endif
  
  while (true) // Creation of the array for SD card file names and their sizes
  {
      File entry = SD_File.openNextFile();
      if (! entry) { // If all file names have been read, exit the while loop
          break;
        }
      if ( ! entry.isDirectory()) {// If it's not a directory
         NomFicSD[i]= entry.name();
         SizeFicSD[i]=(float)entry.size()/1024;
         i++;
         
       }
      entry.close();                  // Close file
  }
  SD_File.close();  
  //*** END of the creation of the array for SD card file names and their sizes         
  Config_WIFI();  
  Alarm_Zero_RTC_Wifi();
  WIFI_STATE = true;
  
  while(WIFI_STATE && WIFI_RTC_STATE) 
  {
    // Compare previous status with current status
    if (status != WiFi.status())
    {   // if different, update status
        status = WiFi.status();
        if (status == WL_AP_CONNECTED)
        { // connecting a peripheral
           // Serial.println("Device connected to the access point");
        } 
        else
        { // device disconnected from access point, return to listening mode
           //Serial.println("Device disconnected from access point");
        }
    } 
 
    WiFiClient client = server.available();   // listening for new clients

    if (client) 
    {                             // if client,
      //Serial.println("new client");       // Displays a message on the serial port    
      String currentLine = "";                // Creation of a String for receiving customer data
      String header;                         // Variable for saving HTTP request
      while (client.connected()) 
      {            // boucle while "as long as the client is connected"
        if (client.available())
        {             // if the client sends bytes,
          char c = client.read();             // read the byte, and 
          //Serial.write(c);                  // display it on the serial port if necessary for debugging purposes
          header += c;                        // header construction = save customer http request if further processing is required
   
         
        if (c == '\n') // // if the byte is a new line character
        {                    
          if (currentLine.length() == 0)
          {  // if the current line is empty, there are two new line characters in a row: 
            // this is the end of the HTTP request from the client, so send a response:                    
            client.println("HTTP/1.1 200 OK"); // the HTTP header always begins with a response code (HTTP/1.1 200 OK)
            client.println("Content-type:text/html"); // and a "Content-type" to specify to the client the type of data it will receive
            client.println(); // and finally an empty line

            // ********** Production of the web page sent to the client *****************

            // ****** Web page header ******           
            client.print("<body style='background-color: #66bac7';><h1> DATALOG : ");
            client.print (STATION_NAME);
            client.println ("</h1><hr>");
            client.print("<hr>");         // horizontal dividing line
            
            // ****** Datalogger status module ******
            client.print("<h2> STATUS of THE DATALOGGER </h2>");                        
            client.print("Time and hour : ");
            Get_RTC();
            for (byte i = 0; i < 6; i++) //write date time
            {
              client.print(RTC_Tab[i]);
              client.print(RTC_Txt[i]);
            }
            client.print("<br>");  
                       
            client.print("Battery level : ");
            Get_Vbatt (&Vbatt); // recuperation de la tension batterie
            client.print(Vbatt);
            client.print(" Volt <br>");
            
            if (Ma_Cadence == 24)
              client.print("One measurement per day <br>");
            else
            {
              client.print("One measurement every ");
              client.print(Ma_Cadence);
              client.print(" min <br>");
            }
            
            #if defined(TYPE_DATALOGGER_HAUTEUR_EAU)
            
              client.print("Distance : ");
              Get_Ht_Eau(&Usound_Dist,&Usound_Temp,&Usound_Noise);// recuperation des données du capteur
              client.print(Usound_Dist);
              client.print(" mm <br>");
  
              client.print("Temperature : ");            
              client.print(Usound_Temp);
              client.print(" C <br>");
  
              client.print("Noise : ");             
              client.print(Usound_Noise);
              client.print(" <br>");

              client.print("Height at zero flow: "); 
              client.print(Ht_Debit_NUL);
              client.print(" mm <br>");

              client.print("Water height: "); 
              client.print(Ht_Debit_NUL - Usound_Dist);
              client.print(" mm <br>");

           
            #elif defined(TYPE_DATALOGGER_ENERGIE)
              if (Capteur_Energie_A0)
              {          
                Get_VEnergie(&IEnergie_0, 0, Capteur_Energie_A0);
                client.print("Current sensor_A0 ");            
                client.print(IEnergie_0);
                client.print(" A / ( Imax = ");
                client.print(Capteur_Energie_A0);
                client.print(" A ) <br>");               
              }
              if (Capteur_Energie_A1) 
              {
                Get_VEnergie(&IEnergie_1, 1, Capteur_Energie_A1);
                client.print("Current sensor__A1 ");            
                client.print(IEnergie_1);
                client.print(" A / ( Imax = ");
                client.print(Capteur_Energie_A1);
                client.print(" A ) <br>");           
              }             
              if (Capteur_Energie_A2)
              {
                 Get_VEnergie(&IEnergie_2, 2, Capteur_Energie_A2);
                 client.print("Current sensor__2 ");            
                 client.print(IEnergie_2);
                 client.print(" A / ( Imax = ");
                 client.print(Capteur_Energie_A2);                 
                 client.print(" A ) <br>");         
              }            
              if (Capteur_Energie_A3)
              {
                Get_VEnergie(&IEnergie_3, 3, Capteur_Energie_A3); 
                client.print("Current sensor__3 ");            
                client.print(IEnergie_3);
                client.print(" A / ( Imax = ");
                client.print(Capteur_Energie_A3);
                client.print(" A ) <br>");          
              }
            
            #elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE)
            
              client.print("Distance : ");
              Get_Ht_Boue(&Ht_Boue_Dist,&Ht_Boue_Temp); // recuperation des données du capteur
              client.print(Ht_Boue_Dist);
              client.print(" cm <br>");
  
              client.print("Temperature : ");            
              client.print(Ht_Boue_Temp);
              client.print(" C <br>");              

              client.print("Empty tank height: "); 
              client.print(Ht_a_Vide);
              client.print(" cm <br>");

              client.print("Height of sludge: "); 
              client.print(Ht_a_Vide - Ht_Boue_Dist);
              client.print(" cm <br>");

            #endif

            client.print("Last Wi-Fi connection : ");            
            client.print(Cwifi);            
            client.print("<br><br>");

            // Refresh button to refresh the page
            client.print("<button onclick='document.location.reload(false)'> Refresh </button>");
            client.print(" <br> ");
            client.print("<hr>");         // horizontal dividing line
            client.print("<hr>");         // horizontal dividing line
            //******* Calibration module for physical-chemical datalogger *****
          #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) //If the physico-chemical datalogger is used    

            client.print("<h2> MEASUREMENT / CALIBRATION </h2>");
           
            client.println("<ul>");
            if (Capteur_PH_A0) // if you have a PH
            {
              client.print("<label> PH display : </label>");
              client.print("<a href=\"/A\">YES</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/B\">NO</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }
            if (PH_Aff) // if you want the PH measurement to be displayed
              {
                client.print("<a href=\"/I\">Start a PH measurement</a>");   // bouton  ON
                client.print(" <br> ");
                if (PH_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  Redox_Aff = false;
                  O2_Aff = false;
                  EC_Aff = false;
                  client.print("PH measure in progress");
                  client.print(" <br> ");
                  Get_Temp(&PH_Temp); // temperature measurement 
                  Sum_Temp = PH_Temp;
                  Get_VPhy(&V_PH_A0, 0, 60); // PH measurement input A0, power-up time 60s
                  Get_Temp(&PH_Temp); // temperature measurement
                  Sum_Temp = PH_Temp + Sum_Temp ; 
                  PH_Temp= Sum_Temp / 2 ;
                  PH_Mes=false;
                } 
                client.print("PH : Temp ");            
                client.print(PH_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");                
                if (C_PH_A == 0.00) // No calculation, calibration required
                  client.print("<br> This probe must be calibrated <br>");                    
                else
                {
                  PH_A0= V_PH_A0-C_PH_B; // calculating PH 
                  PH_A0= PH_A0 / C_PH_A; // calculating PH
                  client.print("PH value ");
                  client.print(PH_A0);
                  client.print(" <br> ");
                } 
                client.print("( PH : Tension ");
                client.print(V_PH_A0);
                client.print(" mV )"); 
                client.print(" <br> ");
                client.print(" <br> ");
                  
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("PH Coeff_A =  ");
                client.print(C_PH_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"phCoeff_A\" id=\"phCoeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("PH Coeff_B =  ");
                client.print(C_PH_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"phCoeff_B\" id=\"phCoeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // horizontal dividing line
            
            if (Capteur_Redox_A1) // if you have a redox probe
            {
              client.print("<label> Affichage Redox : </label>");
              client.print("<a href=\"/Z\">YES</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/D\">NO</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }
             if (Redox_Aff) //If you want the redox probe to be displayed
              {
                client.print("<a href=\"/J\">Start an ORP measurement</a>");   // bouton  ON
                client.print(" <br> ");
                if (Redox_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  PH_Aff = false;
                  O2_Aff = false;
                  EC_Aff = false;
                  client.print("ORP measurement in progress");
                  client.print(" <br> ");
                  Get_Temp(&Redox_Temp); // temperature measurement
                  Sum_Temp = Redox_Temp;
                  Get_VPhy(&V_Redox_A1, 1, 60); //Redox measurement input A1, power-up time 60s
                  Get_Temp(&Redox_Temp); // temperature measurement
                  Sum_Temp = Redox_Temp + Sum_Temp ;
                  Redox_Temp= Sum_Temp / 2 ;
                  Redox_Mes=false;
                }
                client.print("Redox : Temp  ");            
                client.print(Redox_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");                
                //calcul Redox
                if (Redox_A == 0) // No calculation, need calibration
                  client.print("<br> This probe must be calibrated <br>"); 
                else
                {
                  Redox_A1= V_Redox_A1-Redox_B; // Redox calculation
                  Redox_A1= Redox_A1 / Redox_A;  // Redox calculation
                  client.print(" ORP value ");
                  client.print(Redox_A1);
                  client.print(" mV  <br> ");
                }
                client.print("( ORP Tension ");
                client.print(V_Redox_A1);
                client.print(" mV ) "); 
                client.print(" <br> ");
                client.print(" <br> ");
                           
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Redox Coeff_A =  ");
                client.print(Redox_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Redox_Coeff_A\" id=\"Redox_Coeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Redox Coeff_B =  ");
                client.print(Redox_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Redox_Coeff_B\" id=\"Redox_Coeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // horizontal dividing line
            if (Capteur_O2_A2) // if you have an O2 sensor
            {
              client.print("<label> O2 display : </label>");
              client.print("<a href=\"/L\">YES</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/M\">NO</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }
            if (O2_Aff) //If you want the redox probe to be displayed
              {
                client.print("<a href=\"/N\">Start an O2 measurement</a>");   // bouton  ON
                client.print(" <br> ");
                if (O2_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  Redox_Aff = false;
                  PH_Aff = false;
                  EC_Aff = false;
                  client.print("O2 measurement in progress");
                  client.print(" <br> ");
                  Get_Temp(&O2_Temp); // temperature measurement
                  Sum_Temp = O2_Temp;
                  Get_VPhy(&V_O2_A2, 2, 60); //Redox measurement input A2, power-up time 60s
                  Get_Temp(&O2_Temp); // temperature measurement
                  Sum_Temp = O2_Temp + Sum_Temp ;
                  O2_Temp= Sum_Temp / 2 ;
                  O2_Mes=false;
                }
                client.print("O2 : Temp  ");            
                client.print(O2_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");                 
                //calcul O2
                if (O2_A == 0.00) // No calculation, need calibration
                  client.print("<br> This probe must be calibrated <br>"); 
                else
                {
                  O2_A2= V_O2_A2-O2_B; // calcul O2
                  O2_A2= O2_A2 / O2_A; // calcul O2
                  client.print("O2 value ");
                  client.print(O2_A2);
                  client.print(" mg/L  <br> ");
                }
                client.print("( O2 Tension ");
                client.print(V_O2_A2);
                client.print(" mV ) "); 
                client.print(" <br> ");
                client.print(" <br> ");
                           
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("O2 Coeff_A =  ");
                client.print(O2_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"O2_Coeff_A\" id=\"O2_Coeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("O2 Coeff_B =  ");
                client.print(O2_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"O2_Coeff_B\" id=\"O2_Coeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // horizontal dividing line  
            if (Capteur_Conducti_A3)// if you have a Conductivity sensor
            {
              client.print("<label> Conductivity display : </label>");
              client.print("<a href=\"/G\">YES</a>");   // bouton  ON
              client.print(" / ");
              client.print("<a href=\"/H\">NO</a><br><br> ");   // bouton  OFF
              client.print(" <br> ");
            }                                 
                        
              if (EC_Aff) //if you want to display the Conducti
              {
                client.print("<a href=\"/K\">Start a measurement EC</a>");   // bouton  ON
                client.print(" <br> ");
                if (EC_Mes)
                {
                  Alarm_Zero_RTC_Wifi();
                  Redox_Aff = false;
                  O2_Aff = false;
                  PH_Aff = false;
                  client.println("EC measure in progress");
                  client.print(" <br> ");
                  Get_Temp(&Conducti_Temp); // temperature measurement
                  Sum_Temp = Conducti_Temp;
                  Get_VPhy(&V_Conducti_A3, 3, 60); //measure conducti input A3, power-up time 60s
                  Get_Temp(&Conducti_Temp); // temperature measurement
                  Sum_Temp = Conducti_Temp + Sum_Temp ;
                  Conducti_Temp= Sum_Temp / 2 ;
                  EC_Mes=false;
                }
                client.print("Conductivity : Temp  ");            
                client.print(Conducti_Temp);
                client.print(" Deg Celsius");
                client.print(" <br> ");               
               
                // calcul Conducti
                if (Conducti_A == 0) // This probe must be calibrated
                  client.print("<br> This probe must be calibrated <br>"); 
                else
                {
                  Conducti_A3= V_Conducti_A3-Conducti_B; // calculation of Conducti / EC
                  Conducti_A3= Conducti_A3 / Conducti_A; // calculation of Conducti / EC
                  Sum_Temp= 0.020*(Conducti_Temp-25)+1; // calculation of EC temperature correction
                  Conducti_A3 = Conducti_A3 / Sum_Temp ; // calculation of EC temperature correction
                  client.print(" Conductivity value");
                  client.print(Conducti_A3);
                  client.print(" µS/cm  <br> ");
                } 
                client.print("( Conductivity Tension ");
                client.print(V_Conducti_A3);
                client.print(" mV ) "); 
                client.print(" <br> ");
                client.print(" <br> ");
                
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Conducti Coeff_A =  ");
                client.print(Conducti_A);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Conducti_Coeff_A\" id=\"Conducti_Coeff_A\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
                client.print("<li>");
                client.print("<form action=\"/C\" method=\"get\">");
                client.print("Conducti Coeff_B =  ");
                client.print(Conducti_B);
                client.print("  ");
                // client.print("<label for=\"valeur1\"> changer valeur: </label>");
                client.print("<input name=\"Conducti_Coeff_B\" id=\"Conducti_Coeff_B\" type=\"text\" size=\"4\" required=\"true\">");
                client.print("<input type=\"submit\" value=\"Envoyer\"> </a><br>");
                client.print("  ");
                client.print("</form>");
                client.println("</li>");
              }
            client.print(" <br> ");
            client.print("<hr>");         // horizontal dividing line
            client.print("<hr>");         // horizontal dividing line
          #endif 
            // ****** Download module ******

            client.print("<h2> DOWNLOADS </h2>");
  
            // creation of download links bearing the name of the files found 
            for (i=0; i<NbrFic ; i++)
            {
                  client.println("<ul>");
                  client.print("<li><a href=\"");
                  client.print(NomFicSD[i]);
                  client.print("\" >");        
                  client.print(NomFicSD[i]);
                  client.print("</a>");
                  client.print(" ( ");                
                  client.print(SizeFicSD[i]);
                  client.print(" Ko ");
                  client.print(") ");
                  client.print(" <br> <br>");
               
               //*********** start form for processing deletion ************************** NEW **********//
               
                 client.print("<form action=\"/S\" method=\"get\">");
                 client.print("<input name=\"mdp\" id=\"motdepasse\" type=\"text\" inputmode=\"numeric\" size=\"4\" required=\"true\">");
                 client.print("<input name=\"fic\" id=\"fichier\" type=\"hidden\" value=\"");
                 client.print(NomFicSD[i]);
                 client.print("\" >");
                 client.print("<input type=\"submit\" value=\"Delete\"> </a><br>");
                 client.print("<label>type code 5379 to delete the above file</label><br>");
                 client.print("  ");
                 client.print("</form>");    
                       
            
                  client.println("</li>");
                  client.println("</ul>");                
           }           
           client.print(" <br> ");
           client.print("<hr>");         // horizontal dividing line
           client.print("<hr>");         // horizontal dividing line
                
           // ****** SD card status module ******

            client.print("<h2> SD CARD </h2>");

            client.print("Memory : ");
            client.print(EspTotal);
            client.print(" (Go)<br>");
           
            client.print("File memory : ");
            client.print(EspOcc);
            client.print(" (Go) <br>");

            client.print("Free space : ");
            EspLibre = EspTotal - EspOcc ;      // calculate free space on SD card
            client.print(EspLibre);
            client.print(" (Go) <br>");
            
            client.print(" <br> ");
            client.print("<hr>");         // horizontal dividing line                   
            client.print("<hr>");       // horizontal dividing line

            // ***********************************
            
            client.println(); // The HTTP response ends with a new empty line
            break;            // exit the while loop "as long as the client is connected".

            // ********** HTTP response is complete, end of web page sent to client*****************
            
          }          
          else
          {      // If the new line is empty (but the current line is not empty), 
            currentLine = ""; // then update "currentLine" to set it to empty :
          }
        }        
        else if (c != '\r') 
        {    // if the byte is different from a new line and also from a return character ,
          currentLine += c;      // add it to the end of "currentLine
        }

        // If the customer requests a download
        String test="GET /";        
          for (int i=0; i < NbrFic; i++)
          {            
            test +=NomFicSD[i];          
            if (currentLine.startsWith(test))
            {
               currentLine = "";
               Serial.print(" Download request : ");
               Serial.println(NomFicSD[i]);
               client.println("HTTP/1.1 200 OK"); //send new page
               client.println("Content-Disposition: attachment; filename=");
               client.println(NomFicSD[i]);
               client.println("Connection: close");
               client.println(); 
               
               char file_buffer[16];
               int avail;
               SD_File = SD.open(NomFicSD[i], FILE_READ); // open read file
              while (avail = SD_File.available())
              {
                int to_read = min(avail, 16);
                if (to_read != SD_File.read(file_buffer, to_read))
                {
                  break;
                }
                // uncomment the serial to debug (slow!)
                //Serial.write((char)c);
                client.write(file_buffer, to_read);
              }// end of data transmission
              SD_File.close();            
            }            
            test="GET /";
         }
         //if the customer requests file deletion
         String test2 ="GET /S?mdp=5379&fic=";
         if (currentLine.startsWith(test2))
         {            
           boolean RemoveSDFile;
           RemoveSDFile = false;
           for (int i=0; i < NbrFic; i++)
           {
              test2 +=NomFicSD[i];     
              if (currentLine.startsWith(test2))
              {            
                 SD.remove(NomFicSD[i]);
                 test2="GET /S?mdp=5379&fic=";                 
                 for(int j=i; j<NbrFic-1; j++)
                 {
                    NomFicSD[j]=NomFicSD[j+1];
                    SizeFicSD[j]=SizeFicSD[j+1];
                 }
                 i=NbrFic;
                 RemoveSDFile=true;                 
              }
              test2="GET /S?mdp=5379&fic=";
           }
           if( RemoveSDFile)
           {
              NbrFic=NbrFic-1;
              EspTotal= etatSD();   // SD CARD capacity in GB
              EspOcc = espOccSD();  // space occupied SD CARD in GB              
           }                         
          } //end of file deletion request 

          //start processing calibration coefficient changes
        #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE)
          String Coeff_etalonnage ="GET /C?phCoeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1           
              C_PH_A=Coeff.toFloat();
              flash_PH_A.write(C_PH_A);
              Modif_Coeff = true ;                            
           }
           Coeff_etalonnage ="GET /C?phCoeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1          
              C_PH_B= Coeff.toFloat();
              flash_PH_B.write(C_PH_B);
              Modif_Coeff = true ;             
           }
           Coeff_etalonnage ="GET /C?Redox_Coeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1            
              Redox_A=Coeff.toFloat();
              flash_Redox_A.write(Redox_A);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?Redox_Coeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1            
              Redox_B=Coeff.toFloat();
              flash_Redox_B.write(Redox_B);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?O2_Coeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1            
              O2_A=Coeff.toFloat();
              flash_O2_A.write(O2_A);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?O2_Coeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1             
              O2_B=Coeff.toFloat();
              flash_O2_B.write(O2_B);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?Conducti_Coeff_A=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position premier "H" de "HTTP"
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // attribuer valeur1 à la variable SONDE1
              Conducti_A=Coeff.toFloat();
              flash_Conducti_A.write(Conducti_A);
              Modif_Coeff = true ;
           }
           Coeff_etalonnage ="GET /C?Conducti_Coeff_B=";
           if (currentLine.startsWith(Coeff_etalonnage))
           {
              int firstequal = currentLine.indexOf('='); // index position "="
              int firstH = currentLine.indexOf('H');     // index position first "H" in "HTTP
              Coeff = currentLine.substring(firstequal+1,firstH -1);    // assign value1 to variable PROBE1
              Conducti_B=Coeff.toFloat();
              flash_Conducti_B.write(Conducti_B);//writing in flash
              Modif_Coeff = true ;
           }

           // display Measurement Probe 
           if (currentLine.startsWith("GET /A")) 
                {  PH_Aff = true;}
           if (currentLine.startsWith("GET /B"))                        
                {  PH_Aff = false;}
           if (currentLine.startsWith("GET /Z")) 
                {  Redox_Aff = true;}
           if (currentLine.startsWith("GET /D"))                        
                {  Redox_Aff = false;}
           if (currentLine.startsWith("GET /L")) 
                {  O2_Aff = true;}
           if (currentLine.startsWith("GET /M"))                        
                {  O2_Aff = false;}     
           if (currentLine.startsWith("GET /G")) 
                {  EC_Aff = true;}
           if (currentLine.startsWith("GET /H"))                        
                {  EC_Aff = false;} 
           if (currentLine.startsWith("GET /I")) 
                {  PH_Mes = true;} 
           if (currentLine.startsWith("GET /J")) 
                {  Redox_Mes = true;} 
           if (currentLine.startsWith("GET /N")) 
                {  O2_Mes = true;}      
           if (currentLine.startsWith("GET /K")) 
                {  EC_Mes = true;}    
           
        #endif
      
        }
    }
    // end of the while loop, the client is disconnected, close the connection:
    client.stop();
    //Serial.println("client déconnecté");
  }

  // no customer, back to the start of While  
 } 
  WiFi.disconnect(); //Switching off the wifi module
  WiFi.end();
  digitalWrite(STATE_LED_Pin, LOW);
  WIFI_STATE = false;
  Write_WIFI_SD();
  #if defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE)
    if(Modif_Coeff)    
      Write_Coeff_SD(&C_PH_A,&C_PH_B,&Redox_A,&Redox_B,&O2_A,&O2_B,&Conducti_A,&Conducti_B);      //writing new coefficients to SD file 
    
  #endif
  Zero_RTC.detachInterrupt(); 
}

void Config_WIFI()
{
  // Communication Wi-Fi
  //Serial.println(" Serveur Web / Wi-Fi access point ");
  
  // Test module Wi-Fi :
  if (WiFi.status() == WL_NO_MODULE)
  {
    //Serial.println(" Wi-Fi module communication failure !");
    // stop
    while (true);
  }

  //Test version firmware wifi
  String fv = WiFi.firmwareVersion();
  if (fv < WIFI_FIRMWARE_LATEST_VERSION)
  {
    //Serial.println("Update firmware ");
  }

  // Default local IP address = 192.168.4.1, change with : 
  // WiFi.config(IPAddress(10, 0, 0, 1));

  // Display network name (SSID);
 // Serial.print("Access point creation : ");
 // Serial.println(ssid);

  // Create an open network, if necessary a WEP password-protected network:
   //status = WiFi.beginAP(ssid, pass);
  status = WiFi.beginAP(ssid);
  if (status != WL_AP_LISTENING) 
  {
    //Serial.println("Access point creation failure");
    // stop
    while (true);
  }

  // wait 10 seconds for connection:
  delay(10000);

  // Start web server on port 80
  server.begin();

  // connection established, display status
  //printWiFiStatus();  
}

// *********** Fonction printWiFiStatus *********** 
//void printWiFiStatus() {
//  // display network SSID :
//  Serial.print("SSID: ");
//  Serial.println(WiFi.SSID());
//
//  // display IP address of Wifi access point network :
//  IPAddress ip = WiFi.localIP();
//  Serial.print("Adresse IP : ");
//  Serial.println(ip);
//
//  // display on web browser:
//  Serial.print("Connection on this page at http://");
//  Serial.println(ip);
//
//}



// *********** SD card inspection function ************************************************

int inspectSD(){

  //Serial.println("SD card inspection ... ");
  int NbrFichier=0;
  
  // read the number of files on the SD card 
            // incrementally on the root folder "/"
            
            File SD_File1=SD.open("/");
            while (true) {
                File entry = SD_File1.openNextFile();
                if (! entry) { // if all file names have been read, exit the while loop
                    break;
                  }
                if ( ! entry.isDirectory()) {// if not a directory
                   NbrFichier +=1;
                   }
                entry.close();                  // close file
              }
              SD_File1.close();
    
    return NbrFichier;
}

// *********** SD card status function ************************************************

float etatSD(){

  Sd2Card carte;                      // Carte DS
  SdVolume volume;                    //Volume carte SD
  SdFile root;
  uint32_t volumesize;
  
  //Serial.println("Module Web Carte SD ... ");
  
  if (!carte.init(SPI_HALF_SPEED, SD_Pin)) {
  //Serial.println("erreur carte SD");
  while (1);
  } else {
  //Serial.println("carte SD ok");
  }
  
  return (float)carte.cardSize()/2097152; // espace total en Gb

}

// *********** SD card space function ************************************************

float espOccSD(){

  int taille = 0;
  
  // read the size of the files on the SD card 
            // incrementally on the root folder "/"
            
            File SD_File=SD.open("/");
            while (true) {
                File entry = SD_File.openNextFile();
                if (! entry) { // if all file names have been read, exit the while loop
                    break;
                  }
                if ( ! entry.isDirectory()) {// if not a directory
                   taille += entry.size();
                                      
                 }
                entry.close();                  // close file
              }
              SD_File.close();
    
    return (float)taille/2097152; // space occupied in Gb;
}
//*************************************************************

//*****************Fonction Get_Cwifi*****************************************

String Get_Cwifi()
{
  String DateHeure ;      // web page display
  char DateHeure_Tab[18]; // format = 00/00/0000 00:00  --> 16 characters
//  int nbrchar = 0;        // number of characters in file
//  int nbrLigne = 0;       // number of lines in file
  File ficWifi;
  unsigned long Size;
  ficWifi = SD.open("LogWifi.txt"); // Open file
  
  if (ficWifi)// ********* Exploring the file
  
  {    
    Size = ficWifi.size();    
    Size=Size-18;
    ficWifi.seek(Size);
    for (int i = 0; i < 18; i++) // reading date time on last line
    {
        DateHeure_Tab[i] = ficWifi.read();
    }
    
    for (int i = 0; i < 16; i++) // creation du string dateheure
    { //write date time to DateHeure string
      DateHeure = DateHeure + DateHeure_Tab[i];
    }
    ficWifi.close();   // Close file
  } 
  else // error if file does not open    
    DateHeure = " first connection ";  

  return DateHeure;
}
//****************Fonction Get_RTC*******************************************************************

void Get_RTC() //construction of the date-time table
{
   DS1307_Time = DS1307_RTC.now();
   RTC_Tab[2]=DS1307_Time.day();
   RTC_Tab[1]=DS1307_Time.month();
   RTC_Tab[0]=DS1307_Time.year();
   RTC_Tab[3]=DS1307_Time.hour();
   RTC_Tab[4]=DS1307_Time.minute();
   RTC_Tab[5]=DS1307_Time.second();

}
