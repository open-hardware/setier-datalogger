// Program for Datalogger Setier 
// Authors: Héléne GUYARD, Valerie QUATELA, Rémi CLEMENT, Arnold IMIG, Julien SUDRE
// Date: September 2021
// Last update: September 2024
//
// Software: Licensed under the GNU General Public License v3.0
// Hardware: Licensed under CERN-OHL-S v2 or any later version
//
// Boards used for assembly 
// µC board: MKR Wifi 1010 ref: ASX00023
// µSD Board: MKR MEM Shield ref: ASX00008 CS_SD D4 CS_Flash D5
// Backplane: MKR Connector Carrier ref: ASX00007
// RTC Board: GROVE RTC Module DS1307 ref 101020013 (TWI connector)
// Battery voltage reading board: GROVE voltage divider module ref 104020000 (connector A5/A6)
//
// All information on: https://forgemia.inra.fr/open-hardware/setier-datalogger/





//Libraries and .h file required for all types of Base datalogger

#include <ArduinoLowPower.h> //File containing the SAMD21 standby mode functions
#include <Wire.h>   //Library for the I2C
#include "RTClib.h" //Library to use the Grove DS1307 module
#include <RTCZero.h> //Library to use the SAMD21 RTC
#include <SD.h> //caution: this library requires the SPI.h library to be included if it does not exist in the main program.
#include <SPI.h>
#include <WiFiNINA.h> //Library to manage the Wi-Fi module
#include "Config_File.h" // datalogger configuration file 
#include <FlashStorage.h> // using Flash memory to store sensitive data

//Pin and variable definition for basic datalogger
#define STATE_LED_Pin 3  //location status led datalogger

#define Vbatt_Pin A5  // battery voltage measurement module
float Vbatt;   //battery voltage value

FlashStorage(New_Compil, boolean); // Boolean used to detect a new system compilation.

/* Creation of variables for the use of RTCs */
RTC_DS1307 DS1307_RTC; //Creation of the object to use the functions associated with the RTC DS1307
DateTime DS1307_Time; // Creation of the object to store the date and time of the DS1307
RTCZero Zero_RTC; //Creation of the object to use the functions associated with the SAMD21 RTC

/* Definition of the SD card CS pins used with the various cards */
#define SD_Pin 4  //definition of the CS pin on the SD module
File SD_File; //Creation of the SD_File object 
boolean SD_OK; // Variable to determine whether the SD card has been successfully initialized


// RTC modif
int RTC_Tab[6]; // Two tables for easy time display
char RTC_Txt[] = {'/', '/', ' ', ':', ':', ' '};                // modif format


/* Definitions of the different parameters for wifi */


# define WIFI_INTERRUPT_Pin 6

volatile boolean WIFI_STATE ; // Flag for wifi alarm
volatile boolean WIFI_RTC_STATE; // Flag wifi duration

byte Wifi_length; //maximum wifi usage time

char ssid[] = STATION_NAME;         // SSID network (name)
int keyIndex = 0;                   // network key "Index number" (only with WEP)
int status = WL_IDLE_STATUS;

WiFiServer server(80);



/* Definition of the data logger used, according to the name contained in the config file */

#if defined(TYPE_DATALOGGER_HAUTEUR_EAU) //If the water level datalogger is used 
  #include "Ht_Eau.h"  // addition of the relevant tab for calling up sub-programs
  //definition of the global variables used
    word Usound_Dist;
    float Usound_Temp;
    byte Usound_Noise;
    uint16_t Usound_Type;
    uint16_t Usound_Version;
    
#elif defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) // If the physico-chemical datalogger is used
  #include "Physico_Chimique.h" // Include the corresponding tab for calling subroutines
   // Definition of global variables used
    // Variables for the pH sensor connected to A0
    FlashStorage(flash_PH_A, float); // Save pH calibration coefficient A
    FlashStorage(flash_PH_B, float); // Save pH calibration coefficient B
    float PH_Temp; // Temperature associated with pH
    float PH_A0; // Calculated pH value
    float V_PH_A0; // Voltage value of pH
    float C_PH_A; // Calibration coefficient A for pH
    float C_PH_B; // Calibration coefficient B for pH

    // Variables for the Redox sensor connected to A1
    FlashStorage(flash_Redox_A, float); // Save Redox calibration coefficient A
    FlashStorage(flash_Redox_B, float); // Save Redox calibration coefficient B
    float Redox_Temp; // Temperature associated with Redox
    float Redox_A1; // Calculated Redox value
    float V_Redox_A1; // Voltage value of Redox
    float Redox_A; // Calibration coefficient A for Redox
    float Redox_B; // Calibration coefficient B for Redox

    // Variables for the O2 sensor connected to A2
    FlashStorage(flash_O2_A, float); // Save O2 calibration coefficient A
    FlashStorage(flash_O2_B, float); // Save O2 calibration coefficient B
    float O2_Temp; // Temperature associated with O2
    float O2_A2;  // Calculated O2 value
    float V_O2_A2;  // Voltage value of O2
    float O2_A; // Calibration coefficient A for O2
    float O2_B; // Calibration coefficient B for O2
    
    // Variables for the Conductivity sensor connected to A3
    FlashStorage(flash_Conducti_A, float); // Save Conductivity calibration coefficient A
    FlashStorage(flash_Conducti_B, float); // Save Conductivity calibration coefficient B
    float Conducti_Temp; // Temperature associated with conductivity    
    float Conducti_A3;   // Calculated conductivity value
    float V_Conducti_A3;  // Voltage value of conductivity
    float Conducti_A; // Calibration coefficient A for Conductivity
    float Conducti_B; // Calibration coefficient B for Conductivity
 
    float Sum_Temp; // Average of temperature measurements
    
#elif defined(TYPE_DATALOGGER_ENERGIE) // If the energy datalogger is used 
  #include "Energie.h"  // Include the corresponding tab for calling subroutines 
  // Definition of current variables for each energy sensor
    float IEnergie_0;  
    float IEnergie_1; 
    float IEnergie_2; 
    float IEnergie_3;

#elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE) // If the sludge height datalogger is used 
  #include "Ht_Boue.h"  // Include the corresponding tab for calling subroutines 
  // Definition of global variables used   
    float Ht_Boue_Dist;
    float Ht_Boue_Temp;
  
#endif


  

void setup()
{
  
  Serial.begin(115200); // Start the serial connection for the serial monitor

  pinMode(STATE_LED_Pin, OUTPUT); 
  digitalWrite(STATE_LED_Pin, HIGH); // The status LED is on
  Wifi_length=15; // The maximum Wi-Fi usage time is 15 minutes
    
//    while(!Serial) // Waiting for the serial terminal to open
//    {
//      /* Make the LED blink */
//      digitalWrite(STATE_LED_Pin, !digitalRead(STATE_LED_Pin));
//      delay(100);
//    }
//    digitalWrite(STATE_LED_Pin, LOW);

 boolean LedState = true; 
 for (byte i=0;i<90;i++)  // Wait 90 seconds for possible uploading of a new program
 {
    LedState = !LedState;
    digitalWrite(STATE_LED_Pin, LedState); // The status LED is blinking
    delay(1000);
 }
 digitalWrite(STATE_LED_Pin, HIGH); // The status LED is on
 
// Initialization for all Dataloggers
  Init_SD_Card(); // Base SD tab: test for SD card presence
  Init_DS1307_RTC(); // Set the time if compiling (see Base_RTC tab)
  Init_Zero_RTC (); // Initialize the SAMD21 RTC
  New_Compil.write(true); // Compilation boolean set to 1

  // Display the datalogger configuration on the serial monitor after reading the Config_File  
  Serial.println(F("This datalogger will be configured according to the attached config file"));
  Serial.print(F("Datalogger name: ")); 
  Serial.println(STATION_NAME);
  if(Ma_Cadence == 24)
  Serial.println(F("This datalogger will take one measurement per day"));
  else
  {
    Serial.print(F("This datalogger will take a measurement every "));
    Serial.print(Ma_Cadence);
    Serial.println(F(" minutes "));
  }
  
  #if defined(TYPE_DATALOGGER_HAUTEUR_EAU)  // Code compiles if the datalogger is a water height logger
  
    Serial.println(F("This datalogger is of the water height type"));
    Serial.print(F("The distance measured in millimeters at zero flow is: "));
    Serial.println(Ht_Debit_NUL);
    Serial.print(F("The channel depth in millimeters is: "));
    Serial.println(Profondeur_Canal);
    
    Init_Ht_Eau(&Usound_Type,&Usound_Version); // Initialize the sensor (Ht_Eau.h tab)
    Init_SD_Ht_Eau(&Usound_Type,&Usound_Version);
  
    
 #elif defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) // Code compiles if the datalogger is a physico-chemical logger
  
    Serial.println(F("This datalogger is of the physico-chemical type"));
    if (Ma_Cadence == 1)
    {
      Serial.println(F("WARNING: The minimum measurement frequency should be 5 minutes up to 3 sensors, 15 minutes for 4 or more sensors"));      
    }  
    if (Ma_Cadence == 5)
    {
      Serial.println(F("WARNING: With 4 or more sensors, the minimum frequency is 15 minutes"));      
    }  
    if (Capteur_PH_A0) 
    {   
      Serial.println(F("The pH sensor must be connected to A0")); 
      C_PH_A = flash_PH_A.read();
      C_PH_B = flash_PH_B.read();
      if ( C_PH_A == 0.00)
        Serial.println(F("Warning: Calibration coefficient A = 0, the probe needs calibration"));
      if ( C_PH_B == 0.00)
        Serial.println(F("Warning: Calibration coefficient B = 0, the probe needs calibration"));
    }  
    else 
      Serial.println(F("There is no pH sensor"));
      
    if (Capteur_Redox_A1) 
    {   
      Serial.println(F("The Redox sensor must be connected to A1"));
      Redox_A = flash_Redox_A.read();
      Redox_B = flash_Redox_B.read();
      if ( Redox_A == 0.00)
        Serial.println(F("Warning: Calibration coefficient A = 0, the probe needs calibration"));
      if ( Redox_B == 0.00)
        Serial.println(F("Warning: Calibration coefficient B = 0, the probe needs calibration"));
    }      
    else 
      Serial.println(F("There is no Redox sensor"));
      
    if (Capteur_O2_A2)
    {    
      Serial.println(F("The Oxygen sensor must be connected to A2"));
      O2_A = flash_O2_A.read();
      O2_B = flash_O2_B.read();
      if ( O2_A == 0.00)
        Serial.println(F("Warning: Calibration coefficient A = 0, the probe needs calibration"));
      if ( O2_B == 0.00)
        Serial.println(F("Warning: Calibration coefficient B = 0, the probe needs calibration"));      
    }
    else 
      Serial.println(F("There is no Oxygen sensor"));
      
    if (Capteur_Conducti_A3)
    {
      Serial.println(F("The Conductivity sensor must be connected to A3")); 
      Conducti_A = flash_Conducti_A.read();
      Conducti_B = flash_Conducti_B.read();
      if ( Conducti_A == 0.00)
        Serial.println(F("Warning: Calibration coefficient A = 0, the probe needs calibration"));
      if ( Conducti_B == 0.00)
        Serial.println(F("Warning: Calibration coefficient B = 0, the probe needs calibration"));
    }
    else 
      Serial.println(F("There is no Conductivity sensor"));

    V_PH_A0 = V_Redox_A1 = V_O2_A2 = V_Conducti_A3 = 999; // Initialize the sensor voltage variables 
    Init_Relay();  // Initialize the relay board for powering up the sensors   
    Write_Coeff_SD(&C_PH_A,&C_PH_B,&Redox_A,&Redox_B,&O2_A,&O2_B,&Conducti_A,&Conducti_B); // Write the calibration coefficients to the SD card
    
    
 #elif defined(TYPE_DATALOGGER_ENERGIE) // Code compiles if the datalogger is an energy logger
  
    Serial.println(F("This datalogger is of the energy type"));
    if (Capteur_Energie_A0)
    {
      Serial.print(F("Sensor_A0 is of the max current type (in A) "));
      Serial.println(Capteur_Energie_A0);      
    }
    else 
      Serial.println(F("There is no Sensor_A0"));
    if (Capteur_Energie_A1)
    {
      Serial.print(F("Sensor_A1 is of the max current type (in A) "));
      Serial.println(Capteur_Energie_A1);      
    }
    else 
      Serial.println(F("There is no Sensor_A1"));
    if (Capteur_Energie_A2)
    {
      Serial.print(F("Sensor_A2 is of the max current type (in A) "));
      Serial.println(Capteur_Energie_A2);      
    }
    else 
      Serial.println(F("There is no Sensor_A2"));
    if (Capteur_Energie_A3)
    {
      Serial.print(F("Sensor_A3 is of the max current type (in A) "));
      Serial.println(Capteur_Energie_A3);      
    }
    else 
      Serial.println(F("There is no Sensor_A3"));
      
    IEnergie_0 = IEnergie_1 = IEnergie_2 = IEnergie_3 = 999; // Initialize the sensor voltage variables
    Init_Relay(); // Initialize the relay board for powering up the sensors

 #elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE) // Code compiles if the datalogger is a sludge height logger
  
    Serial.println(F("This datalogger is of the sludge height type"));
    Serial.print(F("The distance measured in centimeters when the tank is empty is: "));
    Serial.println(Ht_a_Vide);
    Serial1.begin(19200); // Start the serial connection for the sludge height sensor    
    
 #endif 

  // Configure the Wi-Fi interrupt pin
  pinMode(WIFI_INTERRUPT_Pin, INPUT);
  WIFI_STATE = false;
  LowPower.attachInterruptWakeup(WIFI_INTERRUPT_Pin, WIFI_INTERRUPT_Pg, FALLING);
  
  Serial.print(F("Wi-Fi "));
  Serial.print(STATION_NAME); 
  Serial.println(F(" operational")); 
  digitalWrite(STATE_LED_Pin, LOW);

  // The datalogger is ready to take measurements
}


void loop() 
{
    if (WIFI_STATE == true) // the system was woken up by the WiFi button
      Awake_Wifi(); // call to a subroutine from the Base_Wifi tab
      
    else // Sequential wakeup, beginning of a new measurement cycle
    {   
        digitalWrite(STATE_LED_Pin, HIGH); // The status LED is turned on
        
        #if defined(TYPE_DATALOGGER_HAUTEUR_EAU) // Code is compiled if the datalogger is a water level logger
        
          Get_Vbatt (&Vbatt); // Acquisition of the battery voltage (Base_Vbatt tab)

          // The following lines call subroutines from the Ht_Eau tab
          Get_Ht_Eau(&Usound_Dist, &Usound_Temp, &Usound_Noise); // data acquisition for water height          
          if(Serial) // Display data on the serial monitor
            Affiche_Data_Ht_Eau (&Vbatt, &Usound_Dist, &Usound_Temp, &Usound_Noise);
          if (SD_OK == true) // Write data to the SD card
            Write_Data_SD_Ht_Eau (&Usound_Type, &Usound_Version, &Vbatt, &Usound_Dist, &Usound_Temp, &Usound_Noise);
        
        #elif defined(TYPE_DATALOGGER_PHYSICO_CHIMIQUE) // Code is compiled if the datalogger is a physico-chemical one
        
          Get_Vbatt (&Vbatt); // Acquisition of the battery voltage (Base_Vbatt tab)
          
          // The following lines call subroutines from the Physico_chimique tab
          if (Capteur_PH_A0) // Data acquisition for pH if the sensor is present
          {
            Get_Temp(&PH_Temp); // Measure temperature
            Sum_Temp = PH_Temp;
            Get_VPhy(&V_PH_A0, 0, 30); // pH measurement, A0 input, 30s power-up time
            Get_Temp(&PH_Temp); // Measure temperature
            Sum_Temp = PH_Temp + Sum_Temp; 
            PH_Temp = Sum_Temp / 2;
            // pH calculation 
            if (C_PH_A == 0.00) // No calculation, calibration needed
              PH_A0 = 0;
            else
            {
              PH_A0 = V_PH_A0 - C_PH_B; // pH calculation
              PH_A0 = PH_A0 / C_PH_A; // pH calculation
            }            
          } 
             
          if (Capteur_Redox_A1) // Data acquisition for redox if the sensor is present
          { 
            Get_Temp(&Redox_Temp); // Measure temperature
            Sum_Temp = Redox_Temp;
            Get_VPhy(&V_Redox_A1, 1, 30); // Redox measurement, A1 input, 30s power-up time
            Get_Temp(&Redox_Temp); // Measure temperature
            Sum_Temp = Redox_Temp + Sum_Temp;
            Redox_Temp = Sum_Temp / 2;
            // Redox calculation
            if (Redox_A == 0.00) // No calculation, calibration needed
              Redox_A1 = 0;
            else
            {
              Redox_A1 = V_Redox_A1 - Redox_B; // Redox calculation
              Redox_A1 = Redox_A1 / Redox_A; // Redox calculation
            }            
          }
                       
          if (Capteur_O2_A2) // Data acquisition for oxygen if the sensor is present
          {
            Get_Temp(&O2_Temp); // Measure temperature
            Sum_Temp = O2_Temp;
            Get_VPhy(&V_O2_A2, 2, 30); // O2 measurement, A2 input, 30s power-up time
            Get_Temp(&O2_Temp); // Measure temperature
            Sum_Temp = O2_Temp + Sum_Temp;
            O2_Temp = Sum_Temp / 2;
            // O2 calculation
            if (O2_A == 0.00) // No calculation, calibration needed
              O2_A2 = 0;
            else
            {
              O2_A2 = V_O2_A2 - O2_B; // O2 calculation
              O2_A2 = O2_A2 / O2_A; // O2 calculation
            }            
          }
            
          if (Capteur_Conducti_A3) // Data acquisition for conductivity if the sensor is present
          { 
            Get_Temp(&Conducti_Temp); // Measure temperature
            Sum_Temp = Conducti_Temp;
            Get_VPhy(&V_Conducti_A3, 3, 30); // Conductivity measurement, A3 input, 30s power-up time
            Get_Temp(&Conducti_Temp); // Measure temperature
            Sum_Temp = Conducti_Temp + Sum_Temp;
            Conducti_Temp = Sum_Temp / 2;
            // Conductivity calculation
            if (Conducti_A == 0.00) // No calculation, calibration needed
              Conducti_A3 = 0;
            else
            {
              Conducti_A3 = V_Conducti_A3 - Conducti_B; // Conductivity/EC calculation
              Conducti_A3 = Conducti_A3 / Conducti_A; // Conductivity/EC calculation
              Sum_Temp = 0.020 * (Conducti_Temp - 25) + 1; // Temperature correction for EC
              Conducti_A3 = Conducti_A3 / Sum_Temp; // Temperature correction for EC
            }       
          }          
          if(Serial) // Display data on the serial monitor
            Affiche_Data_Phy (&Vbatt, &PH_Temp, &PH_A0, &V_PH_A0, &Redox_Temp, &Redox_A1, &V_Redox_A1, &O2_Temp, &O2_A2, &V_O2_A2, &Conducti_Temp, &Conducti_A3, &V_Conducti_A3);
          if (SD_OK == true) // Write data to the SD card
            Write_Data_SD_Phy (&Vbatt, &PH_Temp, &PH_A0, &V_PH_A0, &Redox_Temp, &Redox_A1, &V_Redox_A1, &O2_Temp, &O2_A2, &V_O2_A2, &Conducti_Temp, &Conducti_A3, &V_Conducti_A3);
        
        #elif defined(TYPE_DATALOGGER_ENERGIE) // Code is compiled if the datalogger is an energy logger
        
          Get_Vbatt (&Vbatt); // Acquisition of the battery voltage (Base_Vbatt tab)

          // The following lines call subroutines from the Energy tab
          if (Capteur_Energie_A0)          
            Get_VEnergie(&IEnergie_0, 0, Capteur_Energie_A0); // Data acquisition for Energy_0 if present
          if (Capteur_Energie_A1)          
            Get_VEnergie(&IEnergie_1, 1, Capteur_Energie_A1); // Data acquisition for Energy_1 if present 
          if (Capteur_Energie_A2)          
            Get_VEnergie(&IEnergie_2, 2, Capteur_Energie_A2); // Data acquisition for Energy_2 if present 
          if (Capteur_Energie_A3)          
            Get_VEnergie(&IEnergie_3, 3, Capteur_Energie_A3); // Data acquisition for Energy_3 if present           
          if(Serial) // Display data on the serial monitor
            Affiche_Data_Energie (&Vbatt, &IEnergie_0, &IEnergie_1, &IEnergie_2, &IEnergie_3);
          if (SD_OK == true) // Write data to the SD card
            Write_Data_SD_Energie (&Vbatt, &IEnergie_0, &IEnergie_1, &IEnergie_2, &IEnergie_3);


        #elif defined(TYPE_DATALOGGER_HAUTEUR_BOUE) // Code is compiled if the datalogger is a sludge level logger
        
          Get_Vbatt (&Vbatt); // Acquisition of the battery voltage (Base_Vbatt tab)
          
          // The following lines call subroutines from the Ht_Boue tab
          Get_Ht_Boue(&Ht_Boue_Dist, &Ht_Boue_Temp); // Data acquisition for sludge height          
          if(Serial) // Display data on the serial monitor
            Affiche_Data_Ht_Boue (&Vbatt, &Ht_Boue_Dist, &Ht_Boue_Temp);
          if (SD_OK == true) // Write data to the SD card
            Write_Data_SD_Ht_Boue (&Vbatt, &Ht_Boue_Dist, &Ht_Boue_Temp);
                
        #endif 
        
        // Add other dataloggers here
        digitalWrite(STATE_LED_Pin, LOW); // The status LED is turned off
    }    
    delay(1000);
    Alarm_Zero_RTC_Config(); // The alarm is configured again (function in Base_RTC tab) 
    LowPower.sleep();        // The system enters sleep mode 
    Zero_RTC.disableAlarm(); // The system wakes up, the alarm is disabled
}

void WIFI_INTERRUPT_Pg() // Interrupt routine when woken up by the WiFi button
{
   WIFI_STATE = !WIFI_STATE;
}

void Zero_RTC_Interrupt_Pg() // Interrupt routine when woken up sequentially for measurement
{
  WIFI_RTC_STATE = false;
}

