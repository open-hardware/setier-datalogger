/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : confSetierHE.h
# -------------------------------------------------------
#  13/05/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#ifndef CONFSETIERHE_H
#define CONFSETIERHE_H

#include <QDialog>
#include <QDateTime>


namespace Ui {
class ConfSetierHE;
}

class ConfSetierHE : public QDialog
{
    Q_OBJECT

public:
    explicit ConfSetierHE(QWidget *parent = nullptr);
    ~ConfSetierHE();

public slots:
    void ouvrirFichierDATA();
    void ouvrirFichierWIFI();
    void modifchoixDate(int cd);
    void ouvrirVisuSetierHE();
    void updateDEBIT(int TC);

private:
    Ui::ConfSetierHE *ui;

    QString  nomfichierDATA;    // nom du fichier DATA
    QDateTime DT;               // date de debut de courbe
    void init();

};

#endif // CONFSETIERHE_H
