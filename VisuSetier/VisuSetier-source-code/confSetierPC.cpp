/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : confSetierPC.cpp
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/


#include "confSetierPC.h"
#include "ui_confSetierPC.h"
#include "visuSetierPC.h"
#include "visuSetierPCT.h"

#include <QDebug>
#include <QFileDialog>
#include <iostream>

using namespace std;

ConfSetierPC::ConfSetierPC(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfSetierPC)
{
    ui->setupUi(this);
    init();
}

void ConfSetierPC::init()
{

    ui->buttonGroupChoixDate->setId(ui->radioButton_dateinitiale,0);
    ui->buttonGroupChoixDate->setId(ui->radioButton_saisirdate,1);
    ui->buttonGroupChoixDate->setId(ui->radioButton_wifi,2);

    connect(ui->toolButtonDATA,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierDATA()));
    connect(ui->toolButtonETALON,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierETALON()));
    connect(ui->toolButtonWIFI,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierWIFI()));
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(ouvrirVisuSetierPC()));

}

void ConfSetierPC::ouvrirFichierDATA()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditDATA->setText(nomfichier);
            this->nomfichierDATA=nomfichier;
            qDebug() << "nomfichierDATA = "<<nomfichierDATA<< Qt::endl;
        }

}


void ConfSetierPC::ouvrirFichierETALON()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditETALON->setText(nomfichier);
            this->nomfichierETALON=nomfichier;
            qDebug() << "nomfichierETALON = "<<nomfichierETALON<< Qt::endl;
        }

}

void ConfSetierPC::ouvrirFichierWIFI()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditWIFI->setText(nomfichier);
            qDebug() << "nomfichierWIFI = "<<nomfichier<< Qt::endl;

            // recuperer date dans fichier WIFI et remlir la liste graphique des dates
            QFile fic(nomfichier);  //Ouverture du fichier WIFI en lecture

            if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
            {

                QTextStream flux(&fic);
                QString mot;            // mot courant
                QString ligne;          // ligne date
                bool tagStart = false;  // presence tag debut des data
                bool tagEnd = false;    // presence tag fin des data

                while(! flux.atEnd())
                        {
                            flux >> mot;
                            if (mot == "START_DATA")        //début du traitement des valeurs
                            {
                                tagStart = true;
                                while(! flux.atEnd())
                                {
                                    flux >> mot;

                                   if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                                     {
                                        tagEnd = true;
                                        qDebug() << "fin de traitement"<< Qt::endl;
                                        return;
                                   }
                                   else
                                   {
                                       ligne=mot;
                                       flux >> mot;
                                       ligne+=" ";
                                       ligne+=mot;
                                       ui->comboBox->addItem(ligne);
                                   }
                                }
                            }
                }
                qDebug() << "fin de fichier"<< Qt::endl;
                qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;


            }
            else
            {
                cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
            }


        }
}

void ConfSetierPC::modifchoixDate(int cd)
{
    if(cd==0)        // cas courbe complete
    {

        qDebug() << "DT choix 0 "<< Qt::endl;

    }

    if(cd==1)        // cas saisir date
    {
        DT= ui->dateTimeEdit->dateTime() ;
        qDebug() << "DT choix 1 = "<<DT<< Qt::endl;

    }

    if(cd==2)        // cas date fichier WIFI
    {
        // recuperer choix STRING dans liste et transformer en DateTime
        QString datetime_string = ui->comboBox->currentText();
        QDateTime dateT = QDateTime::fromString(datetime_string,"yyyy/MM/dd hh:mm");
        DT= dateT ;
        qDebug() << "DT choix 2 = "<<DT<< Qt::endl;

     }
 }

void ConfSetierPC::ouvrirVisuSetierPC()
{

    // Temperature

    VisuSetierPCT *visuSetierPC_Temp = new VisuSetierPCT;
    visuSetierPC_Temp->setWindowTitle("Temperature");
    visuSetierPC_Temp->setNomfichierDATA(this->nomfichierDATA);
    visuSetierPC_Temp->setChoixDate(ui->buttonGroupChoixDate->checkedId());
    this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
    visuSetierPC_Temp->setdateDebut(this->DT);
    //qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;
    qDebug() << "Temperature"<< Qt::endl;
    visuSetierPC_Temp->traiterFichierSetierPCT();

    visuSetierPC_Temp->show();
    QPoint  position=visuSetierPC_Temp->pos();


    // Capteur_Conductivite

    VisuSetierPC *visuSetierPC_Conducti = new VisuSetierPC;
    visuSetierPC_Conducti->setWindowTitle("Capteur_Conductivite");
    visuSetierPC_Conducti->setNomfichierDATA(this->nomfichierDATA);
    visuSetierPC_Conducti->setNomfichierETALON(this->nomfichierETALON);
    visuSetierPC_Conducti->setChoixDate(ui->buttonGroupChoixDate->checkedId());
    this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
    visuSetierPC_Conducti->setdateDebut(this->DT);
    //qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;
    qDebug() << "Capteur_Conductivite"<< Qt::endl;
    visuSetierPC_Conducti->traiterFichierSetierPC_Conducti();

    position.setX(position.x() + 20);
    position.setY(position.y() + 20);
    visuSetierPC_Conducti->move(position);

    visuSetierPC_Conducti->show();


    // Capteur_O2

    VisuSetierPC *visuSetierPC_O2 = new VisuSetierPC;
    visuSetierPC_O2->setWindowTitle("Capteur_O2");
    visuSetierPC_O2->setNomfichierDATA(this->nomfichierDATA);
    visuSetierPC_O2->setNomfichierETALON(this->nomfichierETALON);
    visuSetierPC_O2->setChoixDate(ui->buttonGroupChoixDate->checkedId());
    this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
    visuSetierPC_O2->setdateDebut(this->DT);
    //qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;
    qDebug() << "Capteur_O2"<< Qt::endl;
    visuSetierPC_O2->traiterFichierSetierPC_O2();

    position.setX(position.x() + 20);
    position.setY(position.y() + 20);
    visuSetierPC_O2->move(position);

    visuSetierPC_O2->show();


    // Capteur_Redox

    VisuSetierPC *visuSetierPC_Redox = new VisuSetierPC;
    visuSetierPC_Redox->setWindowTitle("Capteur_Redox");
    visuSetierPC_Redox->setNomfichierDATA(this->nomfichierDATA);
    visuSetierPC_Redox->setNomfichierETALON(this->nomfichierETALON);
    visuSetierPC_Redox->setChoixDate(ui->buttonGroupChoixDate->checkedId());
    this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
    visuSetierPC_Redox->setdateDebut(this->DT);
    //qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;
    qDebug() << "Capteur_Redox"<< Qt::endl;
    visuSetierPC_Redox->traiterFichierSetierPC_Redox();

    position.setX(position.x() + 20);
    position.setY(position.y() + 20);
    visuSetierPC_Redox->move(position);

    visuSetierPC_Redox->show();


  // Capteur_PH

  VisuSetierPC *visuSetierPC_PH = new VisuSetierPC;
  visuSetierPC_PH->setWindowTitle("Capteur_PH");
  visuSetierPC_PH->setNomfichierDATA(this->nomfichierDATA);
  visuSetierPC_PH->setNomfichierETALON(this->nomfichierETALON);
  visuSetierPC_PH->setChoixDate(ui->buttonGroupChoixDate->checkedId());
  this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
  visuSetierPC_PH->setdateDebut(this->DT);
  //qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;
  qDebug() << "Capteur_PH"<< Qt::endl;
  visuSetierPC_PH->traiterFichierSetierPC_PH();

  position.setX(position.x() + 20);
  position.setY(position.y() + 20);
  visuSetierPC_PH->move(position);

  visuSetierPC_PH->show();


}


ConfSetierPC::~ConfSetierPC()
{
    delete ui;
}
