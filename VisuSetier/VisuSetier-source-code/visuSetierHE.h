/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : visuSetierHE.h
# -------------------------------------------------------
#  13/05/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#ifndef VISUSETIERHE_H
#define VISUSETIERHE_H

#include <QDialog>
#include <iostream>
#include <qwt_plot.h>
#include <qwt_plot_curve.h>
#include <qwt_legend.h>
#include <qwt_plot_grid.h>
#include <qwt_symbol.h>
#include <qwt_date_scale_draw.h>
#include "infofichierSetierHE.h"
#include <qwt_text.h>


using namespace std;

namespace Ui {
class VisuSetierHE;
}

class VisuSetierHE : public QDialog
{
    Q_OBJECT

public:
    explicit VisuSetierHE(QWidget *parent = nullptr);
    ~VisuSetierHE();
    void init();
    void setNomfichierDATA(QString s);
    void setChoixDate(int choixDate);
    void setdateDebut(QDateTime DateTime);
    void setChoixProf(double choixProf);
    void setImportProf(QString importProf);
    void setChoixDist(double choixDist);
    void setImportDist(QString importDist);
    void setCadence(QString cadence);
    void setType(int TC);
    void traiterFichierSetierHE();
    void remplirtabX(QVector <double> tabX);
    void remplirtabY(QVector <double> tabY);
    void remplirtabI(QVector <QString> tabI);
    void remplirtabA(QVector <QString> tabA);
    void afficher(QString labelX, QString labelY);

private:
    Ui::VisuSetierHE *ui;

    QString  nomfichierDATA;
    int type;                   // type canal Venturi 1 à 7
    double ProfR;               // profondeur canal renseignée
    double ProfF;               // profondeur canal importée du fichier DATA
    double DistR;               // distance capteur/fond de canal renseignée
    double DistF;               // distance capteur/fond de canal importée du fichier DATA
    double cadence;             // cadence des mesures
    float a,b,c,d;              // coefficients de calcul du débit Q
    int choixDate;

    QVector <double> dataX;     // tableaux de point X = date/heure
    QVector <double> dataY;     // tableaux de point Y = hauteur d'eau

    QVector <QString> dataI;    // tableaux de I = date Invalide
    QVector <double> dataXDI;   // tableaux de point X Date Invalide : date de mesure
    QVector <double> dataYDI;   // tableaux de point Y Date Invalide : hauteur d'eau = 0

    QVector <QString> dataA;    // tableaux de A = date Alerte
    QVector <double> dataXDA;   // tableaux de point X Date Alerte = date de mesure
    QVector <double> dataYDA;   // tableaux de point Y Date Alerte = hauteur d'eau

    QVector <double> dataY2;     // tableaux de point Y2 = debit

    QDateTime DT;               // date de debut de courbe
    QwtPlot *plotTemp;          // plot Qwt
    QwtPlotCurve *courbe;       // courbe hauteur d'eau
    QwtPlotCurve *courbeDI;     // courbe date invalide
    QwtPlotCurve *courbeDA;     // courbe date Alerte
    QwtPlotCurve *courbeQ;      // courbe debit


    InfoFichierSetierHE *infoFicSetierHE; // fenetre information

    void afficherCourbe(QString labelX, QString labelY, QVector <double> X, QVector <double> Y, QVector<QString> DI, QVector<QString> DA);


private slots:
    void ouvrirInfoFichier();
};

class TimeScaleDraw: public QwtScaleDraw
{

    virtual QwtText label(double v) const
    {
        QDateTime t = QDateTime::fromTime_t((int)v);	//cf fromTime_t
        return t.toString("dd/MM/yyyy\nhh:mm:ss");
    }
};

#endif // VISUSETIERHE_H
