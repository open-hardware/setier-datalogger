/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : mainwindow.h
# -------------------------------------------------------
#  30/11/2022     |   Valérie QUATELA    | Version 0.5 |
##########################################################*/

#ifndef MAINWINDOW_H
#define MAINWINDOW_H


#include <QMainWindow>
#include <QtWidgets>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

public slots:
    void ouvrirConfSetierHE();
    void ouvrirConfSetierPC();
    void ouvrirConfSetierEN();
    void ouvrirApropos();


private:
    Ui::MainWindow *ui;
    void init();

};
#endif // MAINWINDOW_H
