/*#########################################################
#  VISUSETIER
#  Licensed under the GNU General Public License v3.0
#
#  Interface de visualisation de DATA issue de fichiers
#  provenant de capteurs Hydro
#
#	Fichier   : confSetierEN.cpp
# -------------------------------------------------------
#  08/08/2022     |   Valérie QUATELA    | Version 0.4 |
##########################################################*/

#include "confSetierEN.h"
#include "ui_confSetierEN.h"
#include "visuSetierEN.h"

#include <QDebug>
#include <QFileDialog>

ConfSetierEN::ConfSetierEN(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ConfSetierEN)
{
    ui->setupUi(this);
    init();
}


void ConfSetierEN::init()
{

    ui->buttonGroupChoixDate->setId(ui->radioButton_dateinitiale,0);
    ui->buttonGroupChoixDate->setId(ui->radioButton_saisirdate,1);
    ui->buttonGroupChoixDate->setId(ui->radioButton_wifi,2);

    connect(ui->toolButtonDATA,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierDATA()));
    connect(ui->toolButtonWIFI,SIGNAL(clicked(bool)),this,SLOT(ouvrirFichierWIFI()));
    connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(ouvrirVisuSetierEN()));

}

void ConfSetierEN::ouvrirFichierDATA()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditDATA->setText(nomfichier);
            this->nomfichierDATA=nomfichier;
            qDebug() << "nomfichierDATA = "<<nomfichierDATA<< Qt::endl;
        }

}

void ConfSetierEN::ouvrirFichierWIFI()
{
        QString  nomfichier;
        nomfichier = QFileDialog::getOpenFileName( this,"Ouvrir Fichier", " ");
        if (nomfichier.isNull())
            return ;
        if ( ! nomfichier.isEmpty() ){

            ui->lineEditWIFI->setText(nomfichier);
            qDebug() << "nomfichierWIFI = "<<nomfichier<< Qt::endl;

            // recuperer date dans fichier WIFI et remlir la liste graphique des dates
            QFile fic(nomfichier);  //Ouverture du fichier WIFI en lecture

            if(fic.open(QIODevice::ReadOnly | QIODevice::Text))
            {

                QTextStream flux(&fic);
                QString mot;            // mot courant
                QString ligne;          // ligne date
                bool tagStart = false;  // presence tag debut des data
                bool tagEnd = false;    // presence tag fin des data

                while(! flux.atEnd())
                        {
                            flux >> mot;
                            if (mot == "START_DATA")        //début du traitement des valeurs
                            {
                                tagStart = true;
                                while(! flux.atEnd())
                                {
                                    flux >> mot;

                                   if (mot.startsWith("HTTP"))      // fin de traitement en cas de fichier télécharger depuis la carte SD
                                     {
                                        tagEnd = true;
                                        qDebug() << "fin de traitement"<< Qt::endl;
                                        return;
                                   }
                                   else
                                   {
                                       ligne=mot;
                                       flux >> mot;
                                       ligne+=" ";
                                       ligne+=mot;
                                       ui->comboBox->addItem(ligne);
                                   }
                                }
                            }
                }
                qDebug() << "fin de fichier"<< Qt::endl;
                qDebug() << "tagStart = "<<tagStart<< Qt::endl;
                qDebug() << "tagEnd = "<<tagEnd<< Qt::endl;


            }
            else
            {
                cout << "ERREUR: Impossible d'ouvrir le fichier en lecture." << endl;
            }


        }
}

void ConfSetierEN::modifchoixDate(int cd)
{
    if(cd==0)        // cas courbe complete
    {

        qDebug() << "DT choix 0 "<< Qt::endl;

    }

    if(cd==1)        // cas saisir date
    {
        DT= ui->dateTimeEdit->dateTime() ;
        qDebug() << "DT choix 1 = "<<DT<< Qt::endl;

    }

    if(cd==2)        // cas date fichier WIFI
    {
        // recuperer choix STRING dans liste et transformer en DateTime
        QString datetime_string = ui->comboBox->currentText();
        QDateTime dateT = QDateTime::fromString(datetime_string,"yyyy/MM/dd hh:mm");
        DT= dateT ;
        qDebug() << "DT choix 2 = "<<DT<< Qt::endl;

     }
 }

void ConfSetierEN::ouvrirVisuSetierEN()
{

  VisuSetierEN *visuSetierEN = new VisuSetierEN;
  visuSetierEN->setNomfichierDATA(this->nomfichierDATA);
  visuSetierEN->setChoixDate(ui->buttonGroupChoixDate->checkedId());
  this->modifchoixDate(ui->buttonGroupChoixDate->checkedId());
  visuSetierEN->setdateDebut(this->DT);

  qDebug() << "setdateDebut = "<<this->DT<< Qt::endl;

  visuSetierEN->traiterFichierSetierEN();

  visuSetierEN->show();


}


ConfSetierEN::~ConfSetierEN()
{
    delete ui;
}
